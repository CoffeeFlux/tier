---
title: Partial
---

These works have furigana on certain kanji, either due to alternative meanings or if they're uncommon kanji in general, but a good portion of kanji do not have any furigana on them.