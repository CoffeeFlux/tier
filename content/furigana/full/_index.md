---
title: Full
---

All kanji in dialogue bubbles have furigana, but kanji outside of dialogue bubbles are not guaranteed to have any.