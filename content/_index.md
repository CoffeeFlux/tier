---
title: Home
menu:
  main:
    title: "homepage of site"
    weight: -100
---

This site exists just for me to dump a bunch of tier lists onto with arbitrary data. For the most part it exists to rank isekai manga but i could add other types and mix and match everything together if i really wanted to.

If you have suggestions for the site, or something is broken, let me know. I couldn't think of an good way to add comments without making them cancer (furi: tooltips) so i used the detail element to kind of expand it. Might be able to do a weird popout expand box that makes it easier to read and doesn't mess up the flow at some point. Styling is still a work in progress but i'm not putting too much effort into it. If you want to rec something to me to prioritize it to put it on this list as well that would also be cool since there's too much out there to reasonably catch everything that is great.