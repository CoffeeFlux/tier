+++
title = "めざせ豪華客船!!"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=179149"
raw = "https://magazine.jp.square-enix.com/gfantasy/story/mezasegoukakyakusen/"
md = "https://mangadex.org/title/830b4cdd-df77-4a48-975a-aa2bc0684738/striving-for-the-luxury-liner?tab=chapters"
bw = "https://bookwalker.jp/series/299274/list/"

[chapters]
released = 0
read = 13
last_checked = 2022-03-26T00:00:00Z
+++

The protagonist is a little too lustful but i guess it's understandable for someone his age. It's like a comedy without the comedy and the protag isn't interesting. Dude gets summoned to another world with the ability to summon ships so he tries to be an adventurer, fails at that, then tries to be a merchant, but keeps getting scammed but still somehow succeeding since he has cheats anyways.