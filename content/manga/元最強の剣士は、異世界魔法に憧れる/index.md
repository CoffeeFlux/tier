+++
title = "元最強の剣士は、異世界魔法に憧れる"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=147514"
raw = "http://comicride.jp/motosaikyou/"
md = "https://mangadex.org/title/23709/moto-saikyou-no-kenshi-wa-isekai-mahou-ni-akogareru"
bw = "https://bookwalker.jp/series/184401/"

[chapters]
released = 37
read = 22
last_checked = 2021-09-04T00:00:00Z
+++

this is like the new game plus of skill autism, mixed with the 'you can't break these cuffs' meme but with a dude cutting everything