+++
title = "幼女とスコップと魔眼王"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=165523"
raw = "https://seiga.nicovideo.jp/comic/45717"
md = "https://mangadex.org/title/57b8b608-1026-4cab-ad6a-28d796eca663/the-girl-the-shovel-and-the-evil-eye"
bw = "https://bookwalker.jp/series/283911/"

[chapters]
released = 15
read = 14
last_checked = 2021-10-03T00:00:00Z
+++
