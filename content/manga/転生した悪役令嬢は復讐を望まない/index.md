+++
title = "転生した悪役令嬢は復讐を望まない"
tags = []
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=174856"
raw = "https://magcomi.com/episode/13933686331761434251"
md = "https://mangadex.org/title/4c3fc172-fab6-4477-9dfc-57dd95bad19c/the-reincarnated-villainess-doesn-t-want-revenge"
bw = "https://bookwalker.jp/series/298457/"

[chapters]
released = 9
read = 9
last_checked = 2021-09-24T00:00:00Z
+++

I feel jebaited with the protag having twinbraids for most of first chapter and then she never it again. Protag kind of just gets endlessly roped along too. I appreciate the meme furigana sprinkled about occasionally.