+++
title = "サキュバス＆ヒットマン"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=161654"
raw = "https://mangacross.jp/comics/succubus"
md = "https://mangadex.org/title/a90b8d56-4900-41fb-bdbe-49f812fe2807/succubus-hitman"
bw = "https://bookwalker.jp/series/263643"

[chapters]
released = 18
read = 18
last_checked = 2022-01-12T00:00:00Z
+++

Very meme grimdark revenge manga, that is actually fairly enjoyable to read. The comedy lands fairly well and it's paced decently enough to not be boring. Of all of the meme revenge titles of this ilk it's probably the best one.