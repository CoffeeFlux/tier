+++
title = "白の皇国物語"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=105579"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/398000038"
md = "https://mangadex.org/title/87d16991-715f-4a8a-b096-d1f933891ffd/shiro-no-koukoku-monogatari"
bw = "https://bookwalker.jp/series/68095"

[chapters]
released = 64
read = 64
last_checked = 2022-02-01
+++

The early chapters leave out some key details that you have to look closely at the background panels to fully understand a lot of things, which is kind of Fresh in it's own way I suppose if not annoying. After a while a lot of the characters tend to blend together, as the story keeps piling up endless amounts of characters not spending enough time to really develop more than a few, and the character designs suffer from extreme Sameface unfortunately. 

The author is clearly a military otaku who simply wanted to write a story about military strategy and general politics, but decided to do it in an isekai. The world is advanced enough to have basic scientific development, ships, cars, etc, but instead of using fighter jets they decide to just use Dragons that breath fire in the air because despite having guns... because it's a fantasy isekai with magic? Shoutout to the pilots of the dragons actually using fighter jet helmets/suits/gear as well. 

It's honestly a baffling work that doesn't really have any overarching meaning until it's eventually axed.