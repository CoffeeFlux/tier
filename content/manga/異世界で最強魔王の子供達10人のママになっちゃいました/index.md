+++
title = "異世界で最強魔王の子供達10人のママになっちゃいました"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153791"
raw = "https://pocket.shonenmagazine.com/episode/10834108156652672518"
md = "https://mangadex.org/title/36503/i-became-the-mother-of-the-strongest-demon-lord-s-10-children-in-another-world"
bw = "https://bookwalker.jp/series/229419/"

[chapters]
released = 23
read = 23
last_checked = 2021-10-13T00:00:00Z
+++

The first few chapters are kind of...a thing... but it definitely gets a lot better as it goes along. Realdeal 'treasure your family' manga.