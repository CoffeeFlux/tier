+++
title = "四畳半異世界交流記"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146665"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200157010000_68"
md = "https://mangadex.org/title/30662/yojouhan-isekai-kouryuuki"
bw = "https://bookwalker.jp/series/168956/"

[chapters]
released = 14
read = 0
last_checked = 2021-09-04T00:00:00Z
+++
