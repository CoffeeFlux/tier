+++
title = "ヤンデレ系乙女ゲーの世界に転生してしまったようです"
tags = []
categories = ["reincarnation", "isekai", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148230"
raw = ""
md = "https://mangadex.org/title/317837f6-229e-422c-95f3-33b817d74b93/yandere-otome-game"
bw = "https://bookwalker.jp/series/150472"

[chapters]
released = 0
read = 40
last_checked = 2021-10-09T00:00:00Z
+++

moe romance that is pretty similar to most of the other villianess series like hametsu.