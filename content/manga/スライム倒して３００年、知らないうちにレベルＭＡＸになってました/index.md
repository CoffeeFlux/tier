+++
title = "スライム倒して３００年、知らないうちにレベルＭＡＸになってました"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146564"
raw = "https://www.ganganonline.com/contents/slime/"
md = "https://mangadex.org/title/23197/slime-taoshite-300-nen-shiranai-uchi-ni-level-max-ni-nattemashita"
bw = "https://bookwalker.jp/series/146298/"

[chapters]
released = 54.2
read = 29
last_checked = 2021-09-04T00:00:00Z
+++