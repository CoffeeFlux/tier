+++
title = "乙女ゲー世界はモブに厳しい世界です"
statuses = ["ongoing"]
demographics = ["shounen"]
categories = ["game", "isekai"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=151017"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200541010000_68/"
md = "https://mangadex.org/title/31032/the-world-of-otome-games-is-tough-for-mobs"
bw = "https://bookwalker.jp/series/207004/"

[chapters]
released = 35
read = 35
last_checked = 2021-09-04T00:00:00Z
+++

It has issues but i would still say it's enjoyable/good.