+++
title = "公爵令嬢の嗜み"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=131748"
raw = "https://web-ace.jp/youngaceup/contents/1000012/"
md = "https://mangadex.org/title/17720/koushaku-reijou-no-tashinami"
bw = "https://bookwalker.jp/series/82250/"

[chapters]
released = 60.1
read = 59.3
last_checked = 2021-09-04T00:00:00Z
+++