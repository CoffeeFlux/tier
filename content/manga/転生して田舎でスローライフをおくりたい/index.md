+++
title = "転生して田舎でスローライフをおくりたい"
tags = []
categories = ["reincarnation", "isekai"]
demographics = []
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146626"
raw = "http://konomanga.jp/manga/slow-life"
md = "https://mangadex.org/title/b4598b15-822c-48fb-bd18-ee8ae191e381/tensei-shite-inaka-de-slowlife-wo-okuritai"
bw = "https://bookwalker.jp/series/276132/"

[chapters]
released = 48
read = 31
last_checked = 2021-11-08T00:00:00Z
+++

Yet another "protag is really skilled at magic and doesn't really want to exert effort so he goes to live in the countryside in an isekai because he's just Done with black companies in japan but keeps getting roped into random shit so his slowlife isn't like totally slow since that would be a boring manga". If you like the chara design of the sister you might like this more than the other entries in the slowlife cinematic universe.