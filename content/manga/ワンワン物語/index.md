+++
title = "ワンワン物語"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=147467"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02000062010000_68/"
md = "https://mangadex.org/title/473a1d69-0ef5-4882-a45b-ca55c181ce86/wanwan-monogatari-kanemochi-no-inu-n-shite-to-wa-itta-ga-fenrir-ni-shiro-to-wa-itte-nee"
bw = "https://bookwalker.jp/series/177265"

[chapters]
released = 17
read = 12.3
last_checked = 2021-09-20T00:00:00Z
+++

it's like a weird serious comedy where a dude gets reincarnated as a wolf pup and then adopted by a noble girl, then grows up to be massive extremely quickly so you get the juxtaposition of his internal thoughts freaking out how he'll be ostracized for possibly being a Monster and killed, and the oujousama just wanting to cuddle with his fluffyness, while he goes out on epic adventures when nobody sees slaying random enemies that could possibly harm his oujousama in order to protect his comfy pet life since he got abused working at a black company in his previous life (as usual).