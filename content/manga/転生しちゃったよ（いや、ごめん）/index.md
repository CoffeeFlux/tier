+++
title = "転生しちゃったよ（いや、ごめん）"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=126023"
raw = "https://www.alphapolis.co.jp/manga/official/997000115"
md = "https://mangadex.org/title/18642/tensei-shichattayo-iya-gomen"
bw = "https://bookwalker.jp/series/88397/"

[chapters]
released = 65
read = 36
last_checked = 2021-09-04T00:00:00Z
+++

Ah yes the mystical power of *kanji* used as a magic system. eops btfo.