+++
title = "神童セフィリアの下剋上プログラム"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154834"
raw = "https://gammaplus.takeshobo.co.jp/manga/sephiria/"
md = "https://mangadex.org/title/37871/shindou-sefiria-no-gekokujou-program"
bw = "https://bookwalker.jp/series/191516/"

[chapters]
released = 24.2
read = 10
last_checked = 2021-09-04T00:00:00Z
+++

coders rise up.