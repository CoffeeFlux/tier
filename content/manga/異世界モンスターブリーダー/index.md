+++
title = "異世界モンスターブリーダー"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=165476"
raw = "https://seiga.nicovideo.jp/comic/46482"
md = "https://mangadex.org/title/47296/isekai-monster-breeder"
bw = "https://bookwalker.jp/series/253816/"

[chapters]
released = 66
read = 21
last_checked = 2021-09-04T00:00:00Z
+++