+++
title = "メイドから母になりました"
statuses = ["ongoing"]
demographics = ["shoujo"]
tags = []
categories = ["isekai", "reincarnation"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=134541"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/594000134"
md = "https://mangadex.org/title/19454/maid-kara-haha-ni-narimashita"
bw = "https://bookwalker.jp/series/111637/"

[chapters]
released = 56
read = 56
last_checked = 2021-09-04T00:00:00Z
+++