+++
title = "異世界おじさん"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151721"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000079010000_68/"
md = "https://mangadex.org/title/31488/isekai-ojisan"
bw = "https://bookwalker.jp/series/181865/"

[chapters]
released = 35
read = 34
last_checked = 2021-09-04T00:00:00Z
+++
