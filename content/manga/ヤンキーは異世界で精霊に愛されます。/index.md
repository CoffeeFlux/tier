+++
title = "ヤンキーは異世界で精霊に愛されます。"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140113"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/462000159"
md = "https://mangadex.org/title/46889536-a4c3-400e-a892-804d0dc879dc/yankee-wa-isekai-de-seirei-ni-aisaremasu"
bw = "https://bookwalker.jp/series/142269"

[chapters]
released = 42
read = 41
last_checked = 2021-10-24T00:00:00Z
+++

This is runefactory without the farming.