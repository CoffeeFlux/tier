+++
title = "禍つ罠師の勇者狩り"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=158652"
raw = "https://seiga.nicovideo.jp/comic/46357"
md = "https://mangadex.org/title/f3ddc6af-998a-4475-9691-3ca83534f349/wicked-trapper-hunter-of-heroes"
bw = "https://bookwalker.jp/series/244419"

[chapters]
released = 12
read = 11
last_checked = 2021-12-26T00:00:00Z
+++

I appreciate the meme factor of some of the villains just being over the top sex-related, but there's too many Edgy series that just go excessively into gore/sex constantly.