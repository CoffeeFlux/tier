+++
title = "レベル１の最強賢者"
tags = []
categories = ["isekai", "reincarnation"]
demographics = []
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=160098"
raw = "https://comic.pixiv.net/works/6195"
md = "https://mangadex.org/title/b48a41e8-7072-49eb-a5ed-aab1d98f47af/level-1-no-saikyou-kenja-noroi-de-sai-kakyuu-mahou-shika-tsukaenaikedo-kami-no-kanchigai-de-mugen-no-maryoku-o-te-ni-ire-saikyou-ni"
bw = "https://bookwalker.jp/series/255532"

[chapters]
released = 20
read = 20
last_checked = 2021-09-30T00:00:00Z
+++

the twins have an extremely wareyacore design.