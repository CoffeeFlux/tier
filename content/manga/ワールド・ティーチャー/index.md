+++
title = "ワールド・ティーチャー"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=137811"
#raw = ""
md = "https://mangadex.org/title/20288/world-teacher-other-world-style-education-agent"
bw = "https://bookwalker.jp/series/108796/"

[chapters]
released = 49
read = 32
last_checked = 2021-09-04T00:00:00Z
+++

Feels rather uninspired; just a low-conflict story where some Nobles think they're hot shit and beastpeople are slightly discriminated against. The pacing is awkward as well.