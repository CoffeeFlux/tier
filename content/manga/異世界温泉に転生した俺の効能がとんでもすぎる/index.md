+++
title = "異世界温泉に転生した俺の効能がとんでもすぎる"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=143886"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000044010000_68/"
md = "https://mangadex.org/title/957d9031-897c-466f-a626-cc9085899f47/isekai-onsen-ni-tensei-shita-ore-no-kounou-ga-tondemosugiru"
bw = "https://bookwalker.jp/series/137689"

[chapters]
released = 22
read = 22
last_checked = 2021-09-19T00:00:00Z
+++

Ah yes, the iconic hot springs that every japanese isekaijin decides to bring to the new world and try to get everybody to be indoctrinated in the superior japanese bathing culture; Except this time the protag is the onsen, and the premise is that he just looks at naked women 90% of the time. In short, it's nothing but the isekai onsen meme + booba and nothing else. You'd probably be better off looking up onsen porn or something. If you happen to be in the demographic of 'really fucking loves onsen' like every single isekai author seems to belong to then maybe you'd enjoy this.