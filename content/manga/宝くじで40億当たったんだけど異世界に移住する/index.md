+++
title = "宝くじで40億当たったんだけど異世界に移住する"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=138972"
raw = "http://comic-walker.com/contents/detail/KDCW_MF00000030010000_68/"
md = "https://mangadex.org/title/41edb570-19d6-4f01-86ef-081dbcbc1b2e/takarakuji-de-40-oku-atatta-ndakedo-isekai-ni-ijuu-suru"
bw = "https://bookwalker.jp/series/110712"

[chapters]
released = 57
read = 41
last_checked = 2022-01-23T00:00:00Z
+++

One of those subgenres of isekai where it's possible for the protag to go freely back and forth; somewhat solving the need for the protagonist to be a walking-wikipedia when he can just freely go back to his own world and learn things when the issue surfaces (and eventually bring wikipedia back with him) or contract out for various projects to be planned up with his conveniently obtained massive lottery funds. The main highlight of this work is the cute heroines who are also extremely competent in their own respective ways. Like most other series of this kind it starts out as basic village improvement then slowly expands into millitaristic/economic/political topics slowly broadening the reach and spiraling out of control.