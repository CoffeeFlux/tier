+++
title = "異世界でチート能力（スキル）を手にした俺は、現実世界をも無双する"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=161686"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19201336010000_68/"
md = "https://mangadex.org/title/37838287-3fe9-443d-9d22-eafc1089140e/isekai-de-cheat-skill-wo-te-ni-shita-ore-wa-genjitsu-sekai-wo-mo-musou-suru-level-up-wa-jinsei-wo-kaeta"
bw = "https://bookwalker.jp/series/260198"

[chapters]
released = 12
read = 12
last_checked = 2021-10-31T00:00:00Z
+++

This would've been a lot better if he actually had to work to improve himself instead of just immediately becoming Chad in a single night. This is the most power fantasy of power fantasies.