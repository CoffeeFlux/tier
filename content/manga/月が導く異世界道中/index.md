+++
title = "月が導く異世界道中"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=123153"
raw = "http://www.alphapolis.co.jp/manga/official/48000051"
md = "https://mangadex.org/title/16105/tsuki-ga-michibiku-isekai-douchuu"
bw = "https://bookwalker.jp/series/74772/"

[chapters]
released = 66
read = 0
last_checked = 2021-09-04T00:00:00Z
+++