+++
title = "スピリット・マイグレーション"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=118825"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/128000095"
md = "https://mangadex.org/title/34144247-0c21-43d9-a62f-f4143adface0/spirit-migration"
bw = "https://bookwalker.jp/series/104335"

[chapters]
released = 27
read = 27
last_checked = 2022-01-30T00:00:00Z
+++

A story about the protag waking up in an unknown world as an unknown entity with no existing knowledge, trying to interact with everything he meets in whatever way he can to try and find out his purpose. The manga adaptation seems to have been killed off but the WN/LN seem to continue past that so at the very least it could serve to be a good advertisement for those.