+++
title = "出来損ないと呼ばれた元英雄は、実家から追放されたので好き勝手に生きることにした"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156497"
raw = "http://seiga.nicovideo.jp/comic/38765"
md = "https://mangadex.org/title/39854/dekisokonai-to-yobareta-moto-eiyuu-wa-jikka-kara-tsuihousareta-no-de-suki-katte-ni-ikiru-koto-ni-shita"
bw = "https://bookwalker.jp/series/207639/"

[chapters]
released = 24
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

There was a cute twinbraid at least