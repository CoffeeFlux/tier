+++
title = "捨てられた転生賢者"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169083"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666633320"
md = "https://mangadex.org/title/52749/suterareta-tensei-kenja"
bw = "https://bookwalker.jp/series/280580/"

[chapters]
released = 56
read = 19
last_checked = 2021-09-04T00:00:00Z
+++