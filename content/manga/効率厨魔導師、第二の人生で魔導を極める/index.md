+++
title = "効率厨魔導師、第二の人生で魔導を極める"
statuses = ["ongoing"]
demographics = []
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=128683"
raw = "https://www.alphapolis.co.jp/manga/official/69000123"
md = "https://mangadex.org/title/17812/kouritsu-chuu-madoushi-daini-no-jinsei-de-madou-wo-kiwameru"
bw = "https://bookwalker.jp/series/88398/"

[chapters]
released = 54
read = 53
last_checked = 2021-09-04T00:00:00Z
+++