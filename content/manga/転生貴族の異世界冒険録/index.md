+++
title = "転生貴族の異世界冒険録"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=147856"
raw = "https://magcomi.com/episode/10834108156766450183"
md = "https://mangadex.org/title/28612/tensei-kizoku-no-isekai-boukenroku-jichou-wo-shiranai-kamigami-no-shito"
bw = "https://bookwalker.jp/series/181916/"

[chapters]
released = 35
read = 35
last_checked = 2021-09-04T00:00:00Z
+++