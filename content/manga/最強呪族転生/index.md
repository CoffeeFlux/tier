+++
title = "最強呪族転生"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=151456"
raw = "https://comic-earthstar.jp/detail/jyuzoku/"
md = "https://mangadex.org/title/381f7cdf-8eae-4b2c-a8ca-f4298407b6c2/saikyou-juzoku-tensei-cheat-majutsushi-no-slow-life"
bw = "https://bookwalker.jp/series/217981"

[chapters]
released = 20
read = 20
last_checked = 2022-01-27T00:00:00Z
+++

I enjoy this a lot, but it seems to have gone on indefinite hiatus?