+++
title = "圧倒的ガチャ運で異世界を成り上がる！"
tags = []
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=171546"
raw = "https://seiga.nicovideo.jp/comic/49316"
md = "https://mangadex.org/title/ac2088f9-624d-4e80-a244-95c1a33c90f5/attouteki-gacha-un-de-isekai-wo-nariagaru"
bw = "https://bookwalker.jp/defb0e5e9b-6633-44bb-9607-651f52ef37fe/"

[chapters]
released = 20
read = 7
last_checked = 2021-09-17T00:00:00Z
+++

the art occasionally is good but other than that idk