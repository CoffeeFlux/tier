+++
title = "悪役令嬢ルートがないなんて、誰が言ったの？"
tags = []
categories = ["reincarnation", "isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=175947"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03202007010000_68/"
md = "https://mangadex.org/title/ea7a90b2-88f5-400c-b4ea-f30b0c3af19c/there-s-no-such-thing-as-a-villainess-route-not-in-my-book"
bw = "https://bookwalker.jp/dee60d27cf-16be-4cdc-9767-386ba19f7b45/"

[chapters]
released = 10
read = 9
last_checked = 2021-12-29T00:00:00Z
+++

Fairly standard villianess work that's just like every other. Someone 'nice' got reincarnated into the villianess who only can do good, and some homewrecker reincarnated into the Light Magic Commoner who does everything she can do ruin the life of the villianess to make the world the revolve around her. The main relationship between the protag and the hero is cute and pure, so the introduction of the 'light commoner' into that dynamic is just annoying more anything.