+++
title = "悪役令嬢レベル99"
tags = []
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=162806"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201445010000_68/"
md = "https://mangadex.org/title/878634d2-ea39-4001-a4bf-31458020d16a/villainess-level-99-i-may-be-the-hidden-boss-but-i-m-not-the-demon-lord"
bw = "https://bookwalker.jp/series/265215/"

[chapters]
released = 11.2
read = 11.2
last_checked = 2021-09-17T00:00:00Z
+++

It's cute.