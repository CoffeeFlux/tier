+++
title = "異世界に来た僕は器用貧乏で素早さ頼りな旅をする"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=170532"
#raw = ""
md = "https://mangadex.org/title/52170/i-came-to-another-world-as-a-jack-of-all-trades-and-a-master-of-none-to-journey-while-relying-on-quickness"
bw = "https://bookwalker.jp/series/253815/"

[chapters]
released = 23
read = 23
last_checked = 2021-09-04T00:00:00Z
+++

Early chapters have a decent amount of naked/ecchi, then later on that aspect just completely disappears. Doesn't really do anything noteworthy or well but it's not offensively bad.