+++
title = "イキリ勇者は救えない！"
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=176984"
raw = "https://ganma.jp/ikiri"
md = "https://mangadex.org/title/301bc2f1-a338-4eb2-bad6-d714df3d8802/ikiri-yuusha-wa-sukuenai"
bw = "https://bookwalker.jp/series/280072/"

[chapters]
released = 40
read = 7
last_checked = 2021-09-13T00:00:00Z
+++

This is a comedy manga, and it might've been interesting if it was an anime, but i'm not sure how well it works in manga format...