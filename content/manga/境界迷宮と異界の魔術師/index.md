+++
title = "境界迷宮と異界の魔術師"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149230"
raw = "https://comic-gardo.com/episode/10834108156661710153"
md = "https://mangadex.org/title/28536/kyoukai-meikyuu-to-ikai-no-majutsushi"
bw = "https://bookwalker.jp/series/192924/"

[chapters]
released = 39
read = 32
last_checked = 2021-09-04T00:00:00Z
+++

The beginning is just a bizarre speedrun of a story, and the rest of it is just the protag picking up harem members left and right while Owning hilariously evil villains and/or demons. The noble society aspects are largely superficial and not particularly interesting.