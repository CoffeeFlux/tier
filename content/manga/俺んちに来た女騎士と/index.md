+++
title = "俺んちに来た女騎士と"
tags = []
categories = ["isekai", "reverse-isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149691"
raw = "https://www.mangabox.me/reader/78226/episodes/"
md = "https://mangadex.org/title/20e4c17d-3741-46dd-a8bb-cbc132ee52d1/orenchi-ni-kita-onna-kishi-to-inakagurashi-surukotoninatta-ken"
bw = "https://bookwalker.jp/series/189815"

[chapters]
released = 83
read = 79
last_checked = 2021-11-14T00:00:00Z
+++

The early chapters are kind of whatever, but it kind of just becomes more kino as it goes along. A while after that the chapters eventually mellow out a bit as well and also become shorter but otherwise is still p good, worth trying out until the 20s or so.