+++
title = "悪役令嬢転生おじさん"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=162983"
raw = "https://seiga.nicovideo.jp/comic/48434"
md = "https://mangadex.org/title/61b0d21c-b703-4999-910d-ebffc44791bb/akuyaku-reijou-tensei-oji-san"
bw = "https://bookwalker.jp/series/272896"

[chapters]
released = 16
read = 11
last_checked = 2021-09-25T00:00:00Z
+++
