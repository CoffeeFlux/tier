+++
title = "ただ幸せな異世界家族生活"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=177549"
raw = ""
md = "https://mangadex.org/title/54016980-802d-4780-b964-23ee03891b6d/tada-shiawasena-isekai-kazoku-seikatsu"
bw = "https://bookwalker.jp/series/266895/"

[chapters]
released = 11
read = 11
last_checked = 2021-09-16T00:00:00Z
+++

The start of the manga and the first like 3 chapters or so are cool, but i'm not sure if it's the manga's fault or the original source but all of the characters just feel like props for the protag to move at his whims since he's basically god and nobody will ever question him despite being literally a baby. I want to like it but unless it changes in later chapters (however long it will take to reach there) i'm reluctant to raise it higher than this.