+++
title = "転生したらスライムだった件"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=119910"
md = "https://mangadex.org/title/15553/tensei-shitara-slime-datta-ken"
bw = "https://bookwalker.jp/series/56105/"

[chapters]
released = 87
read = 87
last_checked = 2021-09-04T00:00:00Z
+++

I feel like it started decently, but i legit have no idea what it actually wants to do with the series. It all just seems incredibly aimless and written off the cuff.