+++
title = "めっちゃ召喚された件"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=170491"
raw = "https://magcomi.com/episode/13933686331682099976"
md = "https://mangadex.org/title/802bf297-b156-4053-8542-f12057c4a7b9/meccha-shoukan-sareta-ken"
bw = "https://bookwalker.jp/series/275215"

[chapters]
released = 13
read = 11
last_checked = 2021-12-25T00:00:00Z
+++

Edgy chuuni protag gets repeatedly summoned and then eventually ends up in some random world as a 'useless hero' alongside the other actual heroes while being an Epic Badass unknowing to everybody.