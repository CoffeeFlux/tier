+++
title = "群青戦記"
statuses = ["completed"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=103373"
md = "https://mangadex.org/title/12588/gunjou-senki"
bw = "https://bookwalker.jp/series/16059/"

[chapters]
released = 177
read = 0
last_checked = 2021-09-04T00:00:00Z
+++