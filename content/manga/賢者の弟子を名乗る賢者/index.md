+++
title = "賢者の弟子を名乗る賢者"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=134730"
raw = "http://comicride.jp/pupil/"
md = "https://mangadex.org/title/19517/kenja-no-deshi-wo-nanoru-kenja"
bw = "https://bookwalker.jp/series/112079/"

[chapters]
released = 52
read = 30
last_checked = 2021-09-04T00:00:00Z
+++

Honestly a lot of the earlier chapters aren't that interesting but there's a few later on that are decent. Basic starting point, and on a surface level premise, is similar to Overlord.