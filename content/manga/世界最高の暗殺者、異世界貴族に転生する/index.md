+++
title = "世界最高の暗殺者、異世界貴族に転生する"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=152574"
raw = "https://web-ace.jp/youngaceup/contents/1000117"
md = "https://mangadex.org/title/34221/sekai-saikyou-no-assassin-isekai-kizoku-ni-tensei-suru"
bw = "https://bookwalker.jp/series/221175/"

[chapters]
released = 15.1
read = 13
last_checked = 2021-09-04T00:00:00Z
+++