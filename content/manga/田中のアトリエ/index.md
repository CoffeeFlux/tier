+++
title = "田中のアトリエ"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155897"
raw = "https://www.mangabox.me/reader/106226/episodes/"
md = "https://mangadex.org/title/ddfc6303-3026-46bf-af2a-3aca506a6e48/tanaka-no-atelier-nenrei-equal-kanojo-inaireki-no-mahoutsukai"
bw = "https://bookwalker.jp/series/228496"

[chapters]
released = 21
read = 16
last_checked = 2021-12-27T00:00:00Z
+++
