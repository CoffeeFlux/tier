+++
title = "転生侯爵令嬢奮闘記"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=175736"
raw = "https://www.alphapolis.co.jp/manga/official/141000353"
md = "https://mangadex.org/title/2632dd4b-4005-44f9-8a5f-5e1b752e3e42/the-struggle-of-being-reincarnated-as-the-marquess-s-daughter-i-ll-deal-with-what-s-coming-to-me"
bw = "https://bookwalker.jp/series/301303/"

[chapters]
released = 14
read = 13
last_checked = 2022-01-21T00:00:00Z
+++
