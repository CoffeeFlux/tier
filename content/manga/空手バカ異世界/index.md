+++
title = "空手バカ異世界"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153863"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000170010000_68/"
md = "https://mangadex.org/title/250e6bcf-1f12-4cc7-83eb-5fdaf8111dcf/karate-baka-isekai"
bw = "https://bookwalker.jp/series/249854"

[chapters]
released = 18.2
read = 18.1
last_checked = 2022-01-18T00:00:00Z
+++

Karate dude rejects OP cheats as a matter of principle but he's already unhumanly OP naturally so it's kind of pointless. Normal action manga featuring the Power of Superior Martial Arts. Cute booba goddess and twinbraid elf princess tho.