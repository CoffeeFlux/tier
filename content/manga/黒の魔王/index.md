+++
title = "黒の魔王"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["axed"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150723"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000087010000_68/"
md = "https://mangadex.org/title/30728/kuro-no-maou"
bw = "https://bookwalker.jp/series/193348/"

[chapters]
released = 30
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

epic torture porn. epic name. epic naked fairy. epic adventurer's guild (seriously why). comically evil church. it's just endless layers of kuso. There's a few moments of Good mixed in but man it's hard to recommend.