+++
title = "ヘルモード　～やり込み好きのゲーマーは廃設定の異世界で無双する～"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=173268"
raw = "https://www.comic-earthstar.jp/detail/hellmode/"
md = "https://mangadex.org/title/ee4bf782-9fe9-42a6-be7f-3809293818c2/hell-mode-yarikomi-suki-no-gamer-wa-hai-settei-no-isekai-de-musou-suru"
bw = "https://bookwalker.jp/def7687242-81c7-433a-8f9e-15d64c769425/"

[chapters]
released = 14
read = 13
last_checked = 2021-10-29T00:00:00Z
+++

dark souls gamers rise up