+++
title = "魔王軍最強の魔術師は人間だった"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155231"
raw = "https://futabanet.jp/list/monster/work/5dceaa7a7765618ea9090000"
md = "https://mangadex.org/title/39384/maou-gun-saikyou-no-majutsushi-wa-ningen-datta"
bw = "https://bookwalker.jp/series/214322/"

[chapters]
released = 21.1
read = 15
last_checked = 2021-09-04T00:00:00Z
+++

I don't particularly care for the whole "world where demons and humans can live together" standard plotline but if someone did then i'd probably rec them this.