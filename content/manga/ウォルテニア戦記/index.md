+++
title = "ウォルテニア戦記"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
tags = []
categories = ["isekai"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140719"
raw = "https://firecross.jp/ebook/series/232"
md = "https://mangadex.org/title/21130/wortenia-senki"
bw = "https://bookwalker.jp/series/112158/"

[chapters]
released = 41
read = 40
last_checked = 2021-09-04T00:00:00Z
+++

Absolute chad who studied the blade gets summoned out of nowhere and absoutely destroys his summoners before they have a chance to do anything. However he comes across some slaves and saves them and then they basically become his slaves because slaves are just a thing that always happens in isekai (and they're also powerful haha lol). It's kind of odd that the slaves are described as having slightly darker skin but look fully white, and are basically wearing a bikini at all times but what can you do.

It starts to go deep into political intrigue and large war battles filled with strategy, and features a good amount of actually competent women who can hold their own. It also starts to get deeper and deeper and expands more and just becomes pretty cool.