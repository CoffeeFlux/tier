+++
title = "箱庭王国の創造主サマ"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=130595"
raw = ""
md = "https://mangadex.org/title/e081b8a0-5e9d-44cb-af79-75f87b7ceeb4/hakoniwa-oukoku-no-craft-master"
bw = "https://bookwalker.jp/series/81891"

[chapters]
released = 37
read = 19
last_checked = 2021-11-21T00:00:00Z
+++

tfw you create your nekomimi harem kingdom and isekai into it but have to spend most of your time fighting off other random kingdoms of other people that also isekai'd