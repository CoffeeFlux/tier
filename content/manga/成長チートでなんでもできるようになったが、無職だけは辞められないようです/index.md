+++
title = "成長チートでなんでもできるようになったが、無職だけは辞められないようです"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=145875"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000053010000_68/"
md = "https://mangadex.org/title/e881168f-ef99-41ff-88cf-1cf21c152635/seichou-cheat-de-nandemo-dekiru-you-ni-natta-ga-mushoku-dake-wa-yamerarenai-you-desu"
bw = "https://bookwalker.jp/series/154003"

[chapters]
released = 48
read = 38
last_checked = 2021-10-03T00:00:00Z
+++

I thought the art was kind of bad at the start but it definitely grew on me over time, and the plot is similar in that i've grown more attached to it each chapter. キャロ's smile is good tbh. Most of the characters are pretty well-made and memorable. You can really tell the author/artist has a wolfgirl fetish.
