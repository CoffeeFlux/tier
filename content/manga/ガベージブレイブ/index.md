+++
title = "ガベージブレイブ"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156672"
raw = "https://magcomi.com/episode/10834108156766378470"
md = "https://mangadex.org/title/737c292b-5b97-41bb-8b04-51bcc945f7a7/garbage-brave"
bw = "https://bookwalker.jp/series/250985"

[chapters]
released = 20
read = 17
last_checked = 2021-10-09T00:00:00Z
+++

Kind of does a revenge isekai speedrun at the start, then devolves into a bizarre murder mystery where 90% of it is done offscreen.