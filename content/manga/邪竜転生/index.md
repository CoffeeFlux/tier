+++
title = "邪竜転生"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=136131"
raw = "https://www.alphapolis.co.jp/manga/official/7000146"
md = "https://mangadex.org/title/20255/jaryuu-tensei"
bw = "https://bookwalker.jp/series/111659/"

[chapters]
released = 37
read = 33
last_checked = 2021-09-04T00:00:00Z
+++