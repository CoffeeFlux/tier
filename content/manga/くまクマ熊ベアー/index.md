+++
title = "くまクマ熊ベアー"
tags = []
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147991"
raw = "https://pash-up.jp/content/00000001"
md = "https://mangadex.org/title/24244/kuma-kuma-kuma-bear"
bw = "https://bookwalker.jp/series/169130/"

[chapters]
released = 82
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

The art is good, the protag is wholesome, and i really like the alternative perspective chapters that kind of recap the arc/volumes. it's moe.