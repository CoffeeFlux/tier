+++
title = "マギクラフト・マイスター"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=144433"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000049010000_68/"
md = "https://mangadex.org/title/fc2a2247-1a29-48e9-a1c5-b45ed7166c9a/magi-craft-meister"
bw = "https://bookwalker.jp/series/146381"

[chapters]
released = 40
read = 32
last_checked = 2021-10-03T00:00:00Z
+++

imagine if you were an engineer but you could make anything you can think of without really trying as long as you understood the basics of the laws of nature. that's this manga.