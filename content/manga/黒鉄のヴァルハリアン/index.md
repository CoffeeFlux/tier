+++
title = "黒鉄のヴァルハリアン"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=184327"
raw = ""
md = "https://mangadex.org/title/2759019e-5fb7-4f16-8632-a86ea1e4244c/kurogane-no-valhallian"
bw = "https://bookwalker.jp/series/322923/"

[chapters]
released = 0
read = 17
last_checked = 2021-11-26T00:00:00Z
+++

pretty standard 'historical figures battle it out' seinen manga; if you like the others you will probably like this one, although nothing particularly stands out for me in this one from what i've read so far.