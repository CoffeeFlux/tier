+++
title = "盾の勇者の成り上がり"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=107199"
raw = "https://comic-walker.com/contents/detail/KDCW_MF01000012010000_68"
md = "https://mangadex.org/title/12672/tate-no-yuusha-no-nariagari/chapters/"
bw = "https://bookwalker.jp/series/153756/"

[chapters]
released = 78
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

So memes aside the first few chapters with raphtalia are good. It goes quite a fair bit ham onto the Comically Evil Antagonists almost constantly in the early arc(s) but it's mostly fine once it finally moves past that. He even picks up a cute twinbraid along the way 👍 (~~even if she's a furry~~). Honestly i'd say it's worth getting past the early 20ish chapters or so and pushing on because it becomes pretty good past that; i'm looking forward to s2 of the anime now.