+++
title = "極振り拒否して手探りスタート！"
statuses = ["ongoing"]
demographics = []
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=160994"
raw = "https://comic-walker.com/contents/detail/KDCW_FS04201112010000_68/"
md = "https://mangadex.org/title/44405/gokufuri-kyohi-shite-tesaguri-start-tokka-shinai-healer-nakama-to-wakarete-tabi-ni-deru"
bw = "https://bookwalker.jp/series/238542/"

[chapters]
released = 24
read = 20
last_checked = 2021-09-04T00:00:00Z
+++

I do enjoy that the manga is somewhat relatively grounded; Character skills/levels and team comps are fairly low-level (aside from obligatory 'unconventional' OPness of the protag) and just about anything and everything is dangerous. I'm not sure i can rec it in general but it shows promise and the writing isn't garbage so if the descriptions and/or first ep sounds interesting to you may as well try out the rest.