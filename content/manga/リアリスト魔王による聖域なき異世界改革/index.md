+++
title = "リアリスト魔王による聖域なき異世界改革"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153834"
raw = "http://seiga.nicovideo.jp/comic/39394"
md = "https://mangadex.org/title/d6e9b748-9187-4f93-a5df-b802c567f60c/realist-maou-niyoru-seiiki-naki-isekai-kaikaku"
bw = "https://bookwalker.jp/series/212478"

[chapters]
released = 29.1
read = 25
last_checked = 2021-10-03T00:00:00Z
+++

pretty standard demonlord/dungeon plot where random people get summoned and have to build up a dungeon/castle and protect a core, except it's Chad McChadderton and various random heros from earth spreading the gospel of 🇯🇵