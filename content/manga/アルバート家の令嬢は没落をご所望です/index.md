+++
title = "アルバート家の令嬢は没落をご所望です"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148940"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200377010000_68/"
md = "https://mangadex.org/title/ad1fdf0c-eb11-4c52-a657-98f6b4664a41/the-daughter-of-the-albert-house-wishes-for-ruin"
bw = "https://bookwalker.jp/series/193991"

[chapters]
released = 18
read = 11
last_checked = 2021-12-28T00:00:00Z
+++

Great comedy/romance for anybody that is well-versed in standard otomege/villianess tropes, and is probably still really enjoyable without that background knowledge. On a surface level one might be able to compare it to hametsu flag since they're both primarily villianess comedies, but this one forgoes the Full harem experience to instead build upon a small cast of characters and their relationships, and also focuses moreso on trying to actually fall into the ruin route rather than avoid it. Shoutout to https://www.youtube.com/watch?v=A_jwZ6yNjSE (watch after you've read maybe?)