+++
title = "帰ってきた元勇者"
tags = []
categories = ["isekai", "post-isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=157398"
raw = "https://www.mangabox.me/reader/110954/episodes/"
md = "https://mangadex.org/title/8a4ec8d4-ce4e-4654-869b-665cfde2b817/kaettekita-motoyuusha"
bw = "https://bookwalker.jp/series/255557/"

[chapters]
released = 16
read = 7
last_checked = 2021-12-08T00:00:00Z
+++

Protagonist gets summoned to a world, defeats the demon lord, gets sent back to earth, then tries to teleport back to the world he saved so he could have sex with all of the hot girls he wasn't able to have sex with while he was there. Except the world he transported himself to doesn't seem to be the same world he was originally transported to. He still tries to have sex with every girl though. The end.