+++
title = "神様に加護2人分貰いました"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=160615"
raw = "https://www.alphapolis.co.jp/manga/official/995000294"
md = "https://mangadex.org/title/04e50c53-ac8b-4f3b-970b-01d078493afe/kamisama-ni-kago-2-nin-bun-moraimashita"
bw = "https://bookwalker.jp/series/241783"

[chapters]
released = 29
read = 12
last_checked = 2022-01-27T00:00:00Z
+++

Honestly the early chapters are kind of abysmally bad but along the way they stop being bad so it's relatively enjoyable. Maybe.