+++
title = "装備製作系チートで異世界を自由に生きていきます"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=160278"
raw = "https://www.alphapolis.co.jp/manga/official/240000293"
md = "https://mangadex.org/title/42835/i-will-live-freely-in-another-world-with-equipment-manufacturing-cheat"
bw = "https://bookwalker.jp/series/253169/"

[chapters]
released = 13
read = 13
last_checked = 2021-09-04T00:00:00Z
+++

So protag gets like all of the support/manufacturing skills, and instead of like focusing on using those skills to the fullest he instead just, like, becomes an adventurer since that's what mushoku tensei did right, and that's what you do in isekai you rank up in the adventurer's guild. He keeps talking about how his production costs are basically zero, but not taking into account the amount of time it takes to go out and get the materials himself, nor the affect of flooding the market with billions of products will have on the economy. At best you have to completely shut your brain off