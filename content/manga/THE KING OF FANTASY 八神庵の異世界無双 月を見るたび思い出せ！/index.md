+++
title = "THE KING OF FANTASY 八神庵の異世界無双 月を見るたび思い出せ！"
statuses = ["ongoing"]
demographics = ["shounen"]
sexual_contents = [""]
sources = []
furigana = []
categories = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=158698"
raw = "https://comic-walker.com/contents/detail/KDCW_MF09000003010000_68/"
md = "https://mangadex.org/title/43337/the-king-of-fantasy"

[chapters]
released = 26
read = 0
last_checked = 2021-09-04T00:00:00Z
+++