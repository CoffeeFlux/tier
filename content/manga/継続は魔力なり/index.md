+++
title = "継続は魔力なり"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154280"
raw = "https://seiga.nicovideo.jp/comic/39096"
md = "https://mangadex.org/title/cce85f6b-055f-4f6a-83a9-b8c66212bed5/keizoku-wa-maryoku-nari"
bw = "https://bookwalker.jp/series/214656"

[chapters]
released = 14.2
read = 13
last_checked = 2021-09-17T00:00:00Z
+++

it more or less shifts into softcore loliporn at times but other than that it's a relatively standard power fantasy isekai.