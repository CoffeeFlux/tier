+++
title = "転生王女は今日も旗を叩き折る"
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=148942"
raw = "https://bookwalker.jp/series/203603/"
md = "https://mangadex.org/title/35281/the-reincarnated-princess-strikes-down-flags-today-as-well"
bw = "https://bookwalker.jp/series/156281/"
web = "https://arianrose.jp/comic/?series_id=73"

[chapters]
released = 37
read = 20
last_checked = 2021-09-04T00:00:00Z
+++