+++
title = "辺境ぐらしの魔王、転生して最強の魔術師になる"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=171478"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00201820010000_68/"
md = "https://mangadex.org/title/032f800d-d632-4442-8830-d037d616ead4/henkyou-gurashi-no-maou-tensei-shite-saikyou-no-majutsushi-ni-naru"
bw = "https://bookwalker.jp/series/299810/"

[chapters]
released = 12
read = 11
last_checked = 2021-10-09T00:00:00Z
+++
