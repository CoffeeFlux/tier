+++
title = "異世界転生に感謝を"
tags = []
categories = ["isekai", "game", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146026"
raw = ""
md = "https://mangadex.org/title/3236afe9-0a54-4999-91ff-55b693274fbb/isekai-tensei-ni-kansha-o"
bw = "https://bookwalker.jp/series/167781"

[chapters]
released = 14
read = 14
last_checked = 2022-02-06T00:00:00Z 
+++

The core theme of this work is seemingly regret/acceptance, and the protag is a 70yr old man who tries to help out everybody he can with his newfound life that he's obtained in whichever way possible, doling out life lessons. The later chapters are fairly nice but since it got axed abruptly it never ended up going anywhere substantial.