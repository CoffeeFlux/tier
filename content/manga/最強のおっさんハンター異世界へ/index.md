+++
title = "最強のおっさんハンター異世界へ"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155694"
raw = ""
md = "https://mangadex.org/title/0f83e219-f445-4b9b-96d1-dde82d254f3c/saikyou-no-ossan-hunter-isekai-e-kondokoso-yukkuri-shizuka-ni-kurashitai"
bw = "https://bookwalker.jp/series/229858"

[chapters]
released = 24
read = 12
last_checked = 1970-01-01T00:00:00Z
+++

So basically a completionist from the monster hunter universe falls into a hole and gets isekai'd, meets a booba elf and gets brought to her village, slays a dragon, and he's able to go back and forth between the two worlds and is tasked with killing more monsters in the other world.