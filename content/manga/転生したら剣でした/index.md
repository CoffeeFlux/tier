+++
title = "転生したら剣でした"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=138015"
raw = "https://comic-boost.com/series/86"
md = "https://mangadex.org/title/20338/tensei-shitara-ken-deshita"
bw = "https://bookwalker.jp/series/112059/"

[chapters]
released = 49
read = 48
last_checked = 2021-09-04T00:00:00Z
+++