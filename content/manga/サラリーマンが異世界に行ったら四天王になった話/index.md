+++
title = "サラリーマンが異世界に行ったら四天王になった話"
tags = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=160304"
raw = "https://comic-gardo.com/episode/10834108156731296296"
md = "https://mangadex.org/title/45945/salaryman-ga-isekai-ni-ittara-shitennou-ni-natta-hanashi"
bw = "https://bookwalker.jp/series/251534/"

[chapters]
released = 34
read = 0
last_checked = 2021-09-04T00:00:00Z
+++
