+++
title = "生まれ変わった《剣聖》は楽をしたい"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=166748"
raw = "https://comic-gardo.com/episode/13933686331636232737"
md = "https://mangadex.org/title/c491f3c5-5fea-4229-a098-52440e0b67c7/the-reincarnated-sword-saint-wants-to-take-it-easy"
bw = "https://bookwalker.jp/series/264427/"

[chapters]
released = 20
read = 20
last_checked = 2021-09-21T00:00:00Z
+++

I appreciate the focus on teaching/mentoring but the work doesn't really have any meaning or struggles it's just the protag dabbing on people.