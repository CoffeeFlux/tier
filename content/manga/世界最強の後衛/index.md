+++
title = "世界最強の後衛"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=148262"
raw = "http://seiga.nicovideo.jp/comic/33376"
md = "https://mangadex.org/title/b347d551-52eb-40cb-a11a-bfb9959d6495/sekai-saikyou-no-kouei-meikyuukoku-no-shinjin-tansakusha"
bw = "https://bookwalker.jp/series/173879"

[chapters]
released = 0
read = 20
last_checked = 2022-01-18T00:00:00Z
+++

Basically when you die (not of old age?) you get transported to somewhere else and are basically expected to dungeon dive. Except the protag is a salaryman ossan who gets a harem of cute girls to fight on the frontlines for him while he sits in the back really epic.