+++
title = "メイデーア転生物語"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=161897"
raw = "https://magazine.jp.square-enix.com/gfantasy/story/maydare/"
md = "https://mangadex.org/title/9e2707b8-b723-41de-a608-4e4d9ce2f99c/tales-of-reincarnation-in-maydare"
bw = "https://bookwalker.jp/series/258922"

[chapters]
released = 17
read = 17
last_checked = 2021-09-26T00:00:00Z
+++

It's a little too drama-filled and a little too romance-oriented but it doesn't particularly do anything wrong.