+++
title = "異世界で土地を買って農場を作ろう"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155770"
raw = "https://comic-boost.com/series/122"
md = "https://mangadex.org/title/45ed7ed8-23b3-45bf-9b9c-da6e9e88f747/isekai-de-tochi-o-katte-noujou-o-tsukurou"
bw = "https://bookwalker.jp/series/213381/"

[chapters]
released = 30
read = 27
last_checked = 2022-01-20T00:00:00Z
+++
