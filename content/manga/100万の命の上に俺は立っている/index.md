+++
title = "100万の命の上に俺は立っている"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=135499"
raw = "https://pocket.shonenmagazine.com/episode/13932016480029113146"
md = "https://mangadex.org/title/0a817d8e-6458-4332-bdf1-63318e2dc740/100-man-no-inochi-no-ue-ni-ore-wa-tatteiru"
bw = "https://bookwalker.jp/series/86977"

[chapters]
released = 68
read = 54
last_checked = 2021-12-07
+++

kino. the first Quest is kind of whatever but beyond that it just gets progressively more kino. Every Quest basically delves into societal issues or historical events, and the volumes end off with like infodumps about the history in supplementary pages. 

Keep in mind that the work tends to not censor any of the gore/blood/etc that happens and it does happen quite often since it tries to be a near impossible "game" filled with unrealistically hard monsters and quests. The protagonist is also a sociopath.