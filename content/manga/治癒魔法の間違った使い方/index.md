+++
title = "治癒魔法の間違った使い方"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144018"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04000032010000_68/"
md = "https://mangadex.org/title/21946/the-wrong-way-to-use-healing-magic"
bw = "https://bookwalker.jp/series/131858/"

[chapters]
released = 43
read = 43
last_checked = 2021-09-04T00:00:00Z
+++

The characters are good. The character interactions are good. The comedy is good. The story is pretty good. In short it's good thx. Unfortunately the "necromancer arc" that's a little ways in is kind of brainlet; what's the reason for the completely drastic change in quality? There's stills some good parts mixed into it but it's just kind of disappointing. Overally still good though.