+++
title = "異世界の戦士として国に招かれたけど、断って兵士から始める事にした"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=168816"
raw = "https://seiga.nicovideo.jp/comic/50112"
md = "https://mangadex.org/title/50355/i-was-invited-to-join-the-country-as-an-otherworldly-warrior-but-i-refused-and-decided-to-start-as-a-soldier"
az = "https://www.amazon.co.jp/dp/B08R7D2L3Z/"

[chapters]
released = 11
read = 11
last_checked = 2021-09-04T00:00:00Z
+++

The "villainous bad dudes" are once again comically evil to the point that it's not even really interesting. Also shoehorns bizarre discrimination topics constantly. Most other isekai do what this one does, but better unfortunately.