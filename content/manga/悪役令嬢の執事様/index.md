+++
title = "悪役令嬢の執事様"
tags = []
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172105"
raw = "https://www.ganganonline.com/contents/shitsujisama"
md = "https://mangadex.org/title/7722cc2c-cc19-4ca6-ac22-81bce771afe8/akuyaku-reijou-no-shitsuji-sama"
bw = "https://bookwalker.jp/series/283901"

[chapters]
released = 17
read = 17
last_checked = 2022-03-09T00:00:00Z
+++

Protag essentially grooms his waifu he shares a death flag with while they politically maneouver themselves through High Society. I do appreciate that the main heroine isn't just a figurehead who is incapable of doing anything and actually is able to play a role in the story without her Totally Amazing Butler handling everything for her.

I'm rather fond of the characters, the art is nice enough, and the main heroine is pretty.