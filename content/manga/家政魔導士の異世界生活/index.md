+++
title = "家政魔導士の異世界生活"
tags = []
categories = ["isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=158090"
raw = "https://online.ichijinsha.co.jp/zerosum/comic/kasei"
md = "https://mangadex.org/title/700bd275-d17e-4fd6-be65-1994eceaf52d/life-in-another-world-as-a-housekeeping-mage"
bw = "https://bookwalker.jp/series/237987"

[chapters]
released = 22
read = 17
last_checked = 2021-10-29T00:00:00Z
+++
