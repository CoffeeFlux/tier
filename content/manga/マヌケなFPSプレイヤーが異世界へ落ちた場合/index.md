+++
title = "マヌケなFPSプレイヤーが異世界へ落ちた場合"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=130969"
raw = "https://web-ace.jp/youngaceup/contents/1000014"
md = "https://mangadex.org/title/22017/manuke-na-fps-player-ga-isekai-e-ochita-baai"
bw = "https://bookwalker.jp/series/114681/"

[chapters]
released = 22.4
read = 22.3
last_checked = 2021-09-04T00:00:00Z
+++

Did the girl captured by goblins really need to be literally ballgagged.... To be honest the protag being unable to communicate and the character just doing whatever the fuck they want with him is amusing, too bad it only lasts like 3 chapters like every other isekai haha lol. it has an uncanny sense of 'humor' that's like tactless deadpan edgy trauma. The power armor is badass at least.