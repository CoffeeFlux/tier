+++
title = "異世界はスマートフォンとともに"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=143464"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04000031010000_68/"
md = "https://mangadex.org/title/21469/in-another-world-with-my-smartphone"
bw = "https://bookwalker.jp/series/116421/"

[chapters]
released = 58.1
read = 0
last_checked = 2021-09-04T00:00:00Z
+++