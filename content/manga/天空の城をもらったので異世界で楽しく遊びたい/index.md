+++
title = "天空の城をもらったので異世界で楽しく遊びたい"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154759"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04201259010000_68/"
md = "https://mangadex.org/title/38472/tenkuu-no-shiro-o-moratta-no-de-isekai-de-tanoshiku-asobitai"
bw = "https://bookwalker.jp/series/230900/"

[chapters]
released = 26.1
read = 17
last_checked = 2021-09-04T00:00:00Z
+++

The cover art is the best thing about this series.