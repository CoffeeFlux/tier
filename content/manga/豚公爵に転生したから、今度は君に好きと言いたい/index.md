+++
title = "豚公爵に転生したから、今度は君に好きと言いたい"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
tags = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147947"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02200210010000_68/"
md = "https://mangadex.org/title/24206/buta-koushaku-ni-tensei-shitakara-kondo-wa-kimi-ni-suki-to-iitai"
bw = "https://bookwalker.jp/series/173741/"

[chapters]
released = 34
read = 27
last_checked = 2021-09-04T00:00:00Z
+++