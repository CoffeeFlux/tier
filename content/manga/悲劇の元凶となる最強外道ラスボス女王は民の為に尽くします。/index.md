+++
title = "悲劇の元凶となる最強外道ラスボス女王は民の為に尽くします。"
tags = []
categories = ["reincarnation", "game", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["light_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164290"
raw = "http://online.ichijinsha.co.jp/zerosum/comic/rasutame"
md = "https://mangadex.org/title/2c58fb86-7fb9-4db6-9c5f-9696934ea2cb/the-most-heretical-last-boss-queen-who-will-become-the-source-of-tragedy-will-devote-herself-for-the"
bw = "https://bookwalker.jp/series/269575/"

[chapters]
released = 18
read = 18
last_checked = 2022-03-23T00:00:00Z
+++

The art is great and the protagonist is very cute tbh. 

I have to wonder why so many villianess reincarnations believe that fate is unchangeable to the point that they will blindly end up dead from a bad end even while they're actively changing fate. Is there a villianess series where the protagonist actually has to watch on in horror while all of their attempts fail and they're unable to change fate, slowly dreading the day until they die in like some final destination type shit? If so i'd like to read it at least.

This series is mostly about showing the reader horrible despicable events and then the protagonist vowing to not be that horrible of a human and instead doing everything she can to cherish/protect those around her in some kind of weird mood whiplash.