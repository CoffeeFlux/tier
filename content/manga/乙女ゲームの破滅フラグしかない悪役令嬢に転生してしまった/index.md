+++
title = "乙女ゲームの破滅フラグしかない悪役令嬢に転生してしまった"
statuses = ["ongoing"]
demographics = ["josei"]
categories = ["game", "isekai"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=142479"
raw = "http://online.ichijinsha.co.jp/zerosum/comic/hametu"
md = "https://mangadex.org/title/42675/otome-game-no-hametsu-flag-shika-nai-akuyaku-reijou-ni-tensei-shite-shimatta-zettai-zetsumei-hametsu-sunzen-hen"
bw = "https://bookwalker.jp/series/249713/"

[chapters]
released = 41
read = 0
last_checked = 2021-09-04T00:00:00Z
+++