+++
title = "スライム転生。大賢者が養女エルフに抱きしめられてます"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149374"
raw = ""
md = "https://mangadex.org/title/f31297a8-901b-466d-9e46-e5618f2d1663/slime-tensei-daikenja-ga-youjo-elf-ni-dakishimeraretemasu"
bw = "https://bookwalker.jp/series/199100"

[chapters]
released = 32
read = 21.5
last_checked = 2021-09-28T00:00:00Z
+++

It's honestly kind of creepy. Dude basically adopts a bunch of non-human girls, trains them to be powerful, reincarnates as a slime, then goes into baths with them, rubs them with slime tentacles, and just generally being creepy. If you want to self-insert as a slime doting on your adopted daughters from your previous life this is the series for you.