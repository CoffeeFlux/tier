+++
title = "獣医さんのお仕事in異世界"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=141307"
raw = "https://www.alphapolis.co.jp/manga/official/266000135"
md = "https://mangadex.org/title/48592786-4c75-43ed-b697-59f8beb79bc4/jui-san-no-oshigoto-in-isekai"
bw = "https://bookwalker.jp/series/108463"

[chapters]
released = 56
read = 51
last_checked = 2021-12-26T00:00:00Z
+++

The protagonist's only power is his occupation and knowledge, which is typically PHD-level veterinarian knowledge, biology, etc. I feel like the story should've focused a little more on advancing the technology directly instead of going out on Epic shounen-battle-tier fights constantly but it falls in a nice niche regardless.