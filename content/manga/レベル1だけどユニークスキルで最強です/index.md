+++
title = "レベル1だけどユニークスキルで最強です"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=148847"
raw = "http://seiga.nicovideo.jp/comic/34653"
md = "https://mangadex.org/title/27554/level-1-dakedo-unique-skill-de-saikyou-desu"
bw = "https://bookwalker.jp/series/188249/"

[chapters]
released = 37
read = 26
last_checked = 2021-09-04T00:00:00Z
+++

Honestly I kind of like the art, it has a weird kind of charm to it. That's unfortunately the only positive i can think of atm.