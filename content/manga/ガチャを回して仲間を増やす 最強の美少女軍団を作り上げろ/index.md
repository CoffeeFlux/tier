+++
title = "ガチャを回して仲間を増やす 最強の美少女軍団を作り上げろ"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=148073"
raw = "http://comicride.jp/gatya/"
md = "https://mangadex.org/title/24512/gacha-wo-mawashite-nakama-wo-fuyasu-saikyou-no-bishoujo-gundan-wo-tsukuriagero"
bw = "https://bookwalker.jp/series/175789/"

[chapters]
released = 35
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

Honestly this manga doesn't really do anything particularly well; it's just a low-stakes and uninspired problematic gambling manga. appreciate the meme butt angle shots tho. I feel like it's possible to actually do something interesting with this premise, but this isn't it.