+++
title = "落第賢者の学院無双"
statuses = ["ongoing"]
demographics = []
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156932"
raw = "https://magazine.jp.square-enix.com/mangaup/"
md = "https://mangadex.org/title/41754/the-unsuccessful-yet-academically-unparalleled-sage-a-cheating-s-rank-sorcerer-s-post-rebirth-adventurer-log"
bw = "https://bookwalker.jp/series/233430/"

[chapters]
released = 11
read = 11
last_checked = 2021-09-04T00:00:00Z
+++