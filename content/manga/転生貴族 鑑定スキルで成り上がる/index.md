+++
title = "転生貴族 鑑定スキルで成り上がる"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169081"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666634269"
md = "https://mangadex.org/title/51376/reincarnated-as-an-aristocrat-with-an-appraisal-skill"
bw = "https://bookwalker.jp/series/275621/"

[chapters]
released = 48
read = 0
last_checked = 2021-09-04T00:00:00Z
+++