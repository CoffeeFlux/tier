+++
title = "日本国召喚"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150117"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000077010000_68"
md = "https://mangadex.org/title/ef473f14-8c4d-435e-9015-b714512e69eb/nihonkoku-shoukan"
bw = "https://bookwalker.jp/series/188827"

[chapters]
released = 25
read = 25
last_checked = 2021-10-08T00:00:00Z
+++

Every additional page I read decreased my IQ. I never would've imagined something could be more aggressively 🇯🇵 than GATE but the world sure is vast.