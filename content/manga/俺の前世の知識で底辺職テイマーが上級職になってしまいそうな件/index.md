+++
title = "俺の前世の知識で底辺職テイマーが上級職になってしまいそうな件"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=173246"
raw = "https://comic-gardo.com/episode/13933686331724622448"
md = "https://mangadex.org/title/1c83c0e9-c74f-491c-97d8-db0e4e7dc707/the-useless-tamer-will-turn-into-the-top-unconsciously-by-my-previous-life-knowledge"
bw = "https://bookwalker.jp/series/303954/"

[chapters]
released = 12
read = 10
last_checked = 2021-10-09T00:00:00Z
+++
