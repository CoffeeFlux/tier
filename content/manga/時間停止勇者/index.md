+++
title = "時間停止勇者"
statuses = ["ongoing"]
demographics = ["shounen"]
tags = []
categories = ["isekai"]
furigana = ["full"]
sexual_contents = ["nudity"]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156898"
raw = "https://pocket.shonenmagazine.com/episode/10834108156691600068"
md = "https://mangadex.org/title/45729/time-stop-brave"
bw = "https://bookwalker.jp/series/232392/"

[chapters]
released = 22
read = 21
last_checked = 2021-09-04T00:00:00Z
+++

I *really* like how the power is balanced. It manages to put the exact correct amount of restrictions on an absolute cheat ability, and knocks it out of the park every time.