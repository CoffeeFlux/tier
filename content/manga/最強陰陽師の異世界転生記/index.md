+++
title = "最強陰陽師の異世界転生記"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=167354"
raw = "https://futabanet.jp/list/monster/work/5dd4fcf177656139da050000"
md = "https://mangadex.org/title/abd0bc20-063a-45a0-91b2-8b7049701d4a/the-reincarnation-of-the-strongest-onmyoji-these-monsters-are-too-weak-compared-to-my-youkai"
bw = "https://bookwalker.jp/series/250418"

[chapters]
released = 14
read = 9
last_checked = 2021-12-27T00:00:00Z
+++
