+++
title = "乙女ゲーのモブですらないんだが"
tags = []
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165864"
raw = "https://magazine.jp.square-enix.com/mangaup/original/otomegame_mob/"
md = "https://mangadex.org/title/e279d973-e1dd-4cfc-b643-fda857b6f4a0/otome-game-no-mob-desura-naindaga"
bw = "https://bookwalker.jp/series/263569"

[chapters]
released = 12
read = 11
last_checked = 2021-12-28T00:00:00Z
+++

Pretty good moe romance work
