+++
title = "異世界建国記"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=144815"
raw = "https://web-ace.jp/youngaceup/contents/1000046/"
md = "https://mangadex.org/title/22303/isekai-kenkokuki"
bw = "https://bookwalker.jp/series/158920/"

[chapters]
released = 50.2
read = 50.1
last_checked = 2021-09-04T00:00:00Z
+++