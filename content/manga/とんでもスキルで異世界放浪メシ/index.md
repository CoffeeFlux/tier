+++
title = "とんでもスキルで異世界放浪メシ"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=140468"
raw = "https://comic-gardo.com/episode/10834108156661710941"
md = "https://mangadex.org/title/21055/tondemo-skill-de-isekai-hourou-meshi"
bw = "https://bookwalker.jp/series/142109/"

[chapters]
released = 45
read = 44
last_checked = 2021-09-04T00:00:00Z
+++