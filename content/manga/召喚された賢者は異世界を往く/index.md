+++
title = "召喚された賢者は異世界を往く"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156799"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000140010000_68/"
md = "https://mangadex.org/title/52444/shoukan-sareta-kenja-wa-isekai-wo-yuku"
bw = "https://bookwalker.jp/series/219552/"

[chapters]
released = 26.1
read = 22
last_checked = 2021-09-04T00:00:00Z
+++