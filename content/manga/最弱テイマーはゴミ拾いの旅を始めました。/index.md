+++
title = "最弱テイマーはゴミ拾いの旅を始めました。"
tags = []
categories = ["isekai", "reincarnation"]
demographics = []
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162739"
raw = "https://seiga.nicovideo.jp/comic/45503"
md = "https://mangadex.org/title/f0aa2ca6-8985-4b6f-beda-f7fa1956422e/saijaku-tamer-wa-gomi-hiroi-no-tabi-o-hajimemashita"
bw = "https://bookwalker.jp/series/250123"

[chapters]
released = 12.2
read = 10.1
last_checked = 2021-09-30T00:00:00Z
+++

The worldbuilding is good and it's a fairly cute manga overall. Very grounded 'realistic' isekai manga.