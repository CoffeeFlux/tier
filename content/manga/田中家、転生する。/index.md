+++
title = "田中家、転生する。"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169216"
raw = "https://comic-walker.com/contents/detail/KDCW_AM01201802010000_68/"
md = "https://mangadex.org/title/6e9e4d13-1138-4631-9483-860787a9bbad/the-tanaka-family-reincarnates"
bw = "https://bookwalker.jp/series/306835/"

[chapters]
released = 16
read = 16
last_checked = 2021-11-08T00:00:00Z
+++
