+++
title = "異世界帰りの勇者が現代最強！"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153497"
raw = "https://www.ganganonline.com/contents/isekai/"
md = "https://mangadex.org/title/35794/the-hero-who-returned-remains-the-strongest-in-the-modern-world"
bw = "https://bookwalker.jp/series/219018/"

[chapters]
released = 15.3
read = 13.3
last_checked = 2021-09-04T00:00:00Z
+++