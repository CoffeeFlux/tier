+++
title = "神達に拾われた男"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=145806"
raw = "http://www.ganganonline.com/contents/kami/"
md = "https://mangadex.org/title/4187856d-65a9-4fb4-98ec-84c0bf16631f/kamitachi-ni-hirowareta-otoko"
bw = "https://bookwalker.jp/series/164622"

[chapters]
released = 38.1
read = 32
last_checked = 2021-09-24T00:00:00Z
+++

It's a feelgood healing manga and nothing more.