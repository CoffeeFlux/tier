+++
title = "処刑された賢者はリッチに転生して侵略戦争を始める"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169478"
raw = "https://magazine.jp.square-enix.com/mangaup/original/shokei_lich/"
md = "https://mangadex.org/title/c466ced0-ec48-47c1-a8cf-d5ff1fe9d0d2/the-executed-sage-is-reincarnated-as-a-lich-and-starts-an-all-out-war"
bw = "https://bookwalker.jp/series/275434"

[chapters]
released = 16
read = 11
last_checked = 2021-12-28T00:00:00Z
+++

violence begets violence, and war never changes.