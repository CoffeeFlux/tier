+++
title = "ティアムーン帝国物語"
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = ["partial"]
categories = ["reincarnation"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=157049"
raw = "http://seiga.nicovideo.jp/comic/43303"
md = "https://mangadex.org/title/40472/tearmoon-empire-story"
bw = "https://bookwalker.jp/series/230301/"

[chapters]
released = 16.1
read = 13
last_checked = 2021-09-04T00:00:00Z
+++

I'd say the strong point of this series is that while the protagonist is repentant, she's still as brainlet as ever; Magically reincarnating doesn't immediately make you smarter if you never did anything to improve yourself in your last life, and instead of skipping over this process like a lot of series with a Training Montage/arc, it goes all in on the brainlet status. also freckles are justice; Anne-sama i kneel.