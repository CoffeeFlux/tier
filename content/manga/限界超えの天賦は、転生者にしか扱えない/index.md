+++
title = "限界超えの天賦は、転生者にしか扱えない"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=175962"
raw = "https://comic-walker.com/contents/detail/KDCW_AM05201991010000_68/"
md = "https://mangadex.org/title/e57ac6cc-c475-4ef1-a970-b2e261d2ca59/genkai-koe-no-tenpu-wa-tensei-sha-ni-shika-atsukaenai-overlimit-skill-holders"
bw = "https://bookwalker.jp/series/304684/"

[chapters]
released = 9
read = 7
last_checked = 2021-11-21T00:00:00Z
+++
