+++
title = "平兵士は過去を夢見る"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=135109"
raw = "https://www.alphapolis.co.jp/manga/official/357000139"
md = "https://mangadex.org/title/19589/hiraheishi-wa-kako-wo-yumemiru"
bw = "https://bookwalker.jp/series/108462/"

[chapters]
released = 54
read = 52
last_checked = 2021-09-04T00:00:00Z
+++