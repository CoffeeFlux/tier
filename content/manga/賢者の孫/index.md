+++
title = "賢者の孫"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=133460"
raw = "https://web-ace.jp/youngaceup/contents/1000015/"
md = "https://mangadex.org/title/18964/kenja-no-mago"
bw = "https://bookwalker.jp/series/90530/"

[chapters]
released = 62
read = 62
last_checked = 2021-09-04T00:00:00Z
+++

Bonus points for actually seriously progressing the protag's relationship, other characters having their own relationships, and not being a harem. Still meme though.