+++
title = "アラフォー社畜のゴーレムマスター"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149557"
raw = "https://gaugau.futabanet.jp/list/work/5dce87c67765612448040000"
md = "https://mangadex.org/title/37481/arafoo-shachiku-no-golem-master"
bw = "https://bookwalker.jp/series/172341/"

[chapters]
released = 40.1
read = 17
last_checked = 2021-09-04T00:00:00Z
+++