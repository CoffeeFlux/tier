+++
title = "異世界に飛ばされたおっさんは何処へ行く"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146038"
raw = "https://www.alphapolis.co.jp/manga/official/887000196"
md = "https://mangadex.org/title/0855e750-6ada-4c22-bfc5-fa63b1118bda/isekai-ni-tobasareta-ossan-wa-doko-e-iku"
bw = "https://bookwalker.jp/series/170958"

[chapters]
released = 50
read = 33
last_checked = 2022-01-26T00:00:00Z
+++
