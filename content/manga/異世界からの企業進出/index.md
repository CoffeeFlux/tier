+++
title = "異世界からの企業進出"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155528"
raw = "https://seiga.nicovideo.jp/comic/43353"
md = "https://mangadex.org/title/307b3528-2258-4c2e-8235-da8e08a1b4ac/isekai-kara-no-kigyou-shinshutsu"
bw = "https://bookwalker.jp/series/242270/"

[chapters]
released = 15
read = 12
last_checked = 2022-01-18
+++

Unfortunately the artist seemed to have stopped adapting this at some point but it was pretty enjoyable for what was there. The web novel is still updating but i'm not all that interested in checking it out though unfortunately