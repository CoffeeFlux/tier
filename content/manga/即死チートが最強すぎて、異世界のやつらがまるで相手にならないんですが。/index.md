+++
title = "即死チートが最強すぎて、異世界のやつらがまるで相手にならないんですが。"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
tags = []
categories = ["isekai"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148724"
raw = "http://comic-earthstar.jp/detail/sokushicheat/"
md = "https://mangadex.org/title/24281/sokushi-cheat-ga-saikyou-sugite-isekai-no-yatsura-ga-marude-aite-ni-naranai-n-desu-ga"
bw = "https://bookwalker.jp/series/169748/"

[chapters]
released = 31
read = 31
last_checked = 2021-09-04T00:00:00Z
+++

Decently done low-stakes comedy manga that is actually more than that and really cool and i love it actually it's great.