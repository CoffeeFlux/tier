+++
title = "クラスが異世界召喚されたなか俺だけ残ったんですが"
tags = []
categories = ["isekai", "reverse-isekai"]
demographics = ["seinen"]
statuses = ["hiatus"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=145960"
raw = "https://comic-boost.com/series/89"
md = "https://mangadex.org/title/2e3d0203-e1d2-4142-b6ab-69a70643d7a1/class-ga-isekai-shoukan-sareta-naka-ore-dake-nokotta-n-desu-ga"
bw = "https://bookwalker.jp/series/165917"

[chapters]
released = 32
read = 30
last_checked = 2022-01-27T00:00:00Z
+++
