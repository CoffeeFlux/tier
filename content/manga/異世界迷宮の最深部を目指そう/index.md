+++
title = "異世界迷宮の最深部を目指そう"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155332"
raw = "https://comic-gardo.com/episode/10834108156668670658"
md = "https://mangadex.org/title/39510/isekai-meikyuu-no-saishinbu-o-mezasou"
bw = "https://bookwalker.jp/series/233711/"

[chapters]
released = 20
read = 17
last_checked = 2021-09-04T00:00:00Z
+++

The protag is interesting, he has a severe complex about the world and his thoughts of what he should be doing. dia is pretty cool as well; Her complex with wanting to be the ideal shounen protagonist and general gender identity is moe. The characters carry this series hard and the world itself is decent enough that this is kind of pretty cool.