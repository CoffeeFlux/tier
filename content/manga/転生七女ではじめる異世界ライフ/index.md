+++
title = "転生七女ではじめる異世界ライフ"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=182997"
raw = ""
md = "https://mangadex.org/title/2193e82e-ae7c-4f5e-b498-f96090784688/tensei-nanajou-de-hajimeru-isekai-life"
bw = "https://bookwalker.jp/series/323597/"

[chapters]
released = 0
read = 8
last_checked = 2021-12-29T00:00:00Z
+++

Maybe you'd like this if you were very into yuri but it doesn't do much at this point beyond that.