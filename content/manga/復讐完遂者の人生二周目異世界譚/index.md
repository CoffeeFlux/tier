+++
title = "復讐完遂者の人生二周目異世界譚"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153322"
raw = "http://comicride.jp/avenger/"
md = "https://mangadex.org/title/35479/fukushuu-kansuisha-no-jinsei-nishuume-isekaitan"
bw = "https://bookwalker.jp/series/223291/"

[chapters]
released = 27
read = 19
last_checked = 2021-09-04T00:00:00Z
+++