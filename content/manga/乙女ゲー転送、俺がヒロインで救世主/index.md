+++
title = "乙女ゲー転送、俺がヒロインで救世主"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156669"
raw = "https://seiga.nicovideo.jp/comic/46917"
md = "https://mangadex.org/title/3ef126d1-5e41-4594-924b-cb6e5ca99bdb/isekai-tensei-ore-ga-otome-ge-de-kyuuseishu"
bw = "https://bookwalker.jp/de6b9adc23-2539-41c9-b9bf-4f94ad05a6e7"

[chapters]
released = 17
read = 16
last_checked = 2021-10-01T00:00:00Z
+++

it's a cool series that mixes the otomege/villianess genres with general male-protag isekai with a focus on comedy and it's kind of a really cool blend of josei and shounen.