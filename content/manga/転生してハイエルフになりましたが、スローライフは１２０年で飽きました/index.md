+++
title = "転生してハイエルフになりましたが、スローライフは１２０年で飽きました"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=184171"
raw = "https://comic.pixiv.net/works/7610"
md = "https://mangadex.org/title/8280b28c-aeff-4f89-ac99-a4c783e14782/growing-tired-of-the-lazy-high-elf-life-after-120-years"
bw = "https://bookwalker.jp/series/336697/list/"

[chapters]
released = 8
read = 8
last_checked = 2022-02-20T00:00:00Z
+++

The protag basically just chills out slowly doing his isekai tourism while meeting new people and changing their lives.