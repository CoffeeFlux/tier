+++
title = "異世界転生騒動記"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
tags = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=122360"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/552000104"
md = "https://mangadex.org/title/15359/isekai-tensei-soudouki"
bw = "https://bookwalker.jp/series/68106/"

[chapters]
released = 70
read = 69
last_checked = 2021-09-04T00:00:00Z
+++