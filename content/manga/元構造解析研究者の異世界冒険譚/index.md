+++
title = "元構造解析研究者の異世界冒険譚"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=166520"
raw = "https://www.alphapolis.co.jp/manga/official/92000221"
md = "https://mangadex.org/title/e7c88847-9a61-4856-94db-fefae51a1d28/the-former-structural-analyst-s-otherworldly-adventure-story"
bw = "https://bookwalker.jp/series/201569"

[chapters]
released = 39
read = 15
last_checked = 2021-09-17T00:00:00Z
+++
