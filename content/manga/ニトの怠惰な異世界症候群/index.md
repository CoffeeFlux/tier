+++
title = "ニトの怠惰な異世界症候群"
tags = []
categories = ["isekai", "reincarnation"]
demographics = []
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=158142"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000310010000_68/"
md = "https://mangadex.org/title/30eb9ee7-4cfe-4036-95f9-4aac5e05b505/nito-no-taidana-isekai-shoukougun-saijaku-shoku-healer-nano-ni-saikyou-wa-cheat-desu-ka"
bw = "https://bookwalker.jp/series/245039"

[chapters]
released = 17.2
read = 16.2
last_checked = 2021-10-07T00:00:00Z
+++

What's the point of having excessively over the top revenge stories if it more or less drops the revenge after a few chapters and just becomes happy harem homelife.