+++
title = "千のスキルを持つ男"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=152778"
raw = "https://polca.123hon.com/book_series/1000skills/"
md = "https://mangadex.org/title/61e5cfcb-aff4-4916-ac20-132f4542e451/sen-no-sukiru-o-motsu-otoko"
bw = "https://bookwalker.jp/series/211390"

[chapters]
released = 0
read = 37
last_checked = 2021-10-02T00:00:00Z
+++

odd to come across another series that uses 3d models for characters. it's fairly unremarkable honestly since it doesn't really do anything interesting with the premise it set out.