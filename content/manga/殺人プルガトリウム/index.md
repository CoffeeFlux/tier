+++
title = "殺人プルガトリウム"
tags = []
categories = ["reincarnation"]
demographics = ["josei"]
statuses = ["completed"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://mangahot.jp/site/works/t_R0002"
raw = "https://www.mangaupdates.com/series.html?id=152481"
md = "https://mangadex.org/title/6294a2e4-315d-4258-bca5-b4f309827759/satsujin-purgatorium"
bw = "https://bookwalker.jp/series/189779/"

[chapters]
released = 23.5
read = 23.5
last_checked = 2021-10-24T00:00:00Z
+++

The balance feels kind of all over the place, it ends in a kind of unsatisfying way, the mechanics of the world are pretty obvious but the characters feel the need to constantly spell it out, it spends a little too much time on Epic action scenes involving shooting/killing/etc.