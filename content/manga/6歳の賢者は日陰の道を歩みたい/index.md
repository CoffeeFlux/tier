+++
title = "6歳の賢者は日陰の道を歩みたい"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=174549"
raw = "https://www.ganganonline.com/contents/rokusai/"
md = "https://mangadex.org/title/9913f21f-d531-458f-a980-c6657981f0cd/6-sai-no-kenja-wa-hikage-no-michi-wo-ayumitai"
bw = "https://bookwalker.jp/series/258914/"

[chapters]
released = 12
read = 12
last_checked = 2021-10-21T00:00:00Z
+++
