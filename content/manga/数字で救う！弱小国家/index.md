+++
title = "数字で救う！弱小国家"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155349"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02201046010000_68/"
md = "https://mangadex.org/title/39819/suuji-de-sukuu-jyakushou-kokka"
bw = "https://bookwalker.jp/series/233851/"

[chapters]
released = 25
read = 0
last_checked = 2021-09-04T00:00:00Z
+++
