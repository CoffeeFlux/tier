+++
title = "神眼の勇者"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=144994"
raw = "http://seiga.nicovideo.jp/comic/34131"
md = "https://mangadex.org/title/22372/shingan-no-yuusha"
bw = "https://bookwalker.jp/series/148906/"

[chapters]
released = 39.2
read = 31
last_checked = 2021-09-04T00:00:00Z
+++

The humor is good.