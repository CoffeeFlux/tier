+++
title = "魔王になったので、ダンジョン造って人外娘とほのぼのする"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149456"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200334010000_68/"
md = "https://mangadex.org/title/26297/maou-ni-natta-node-dungeon-tsukutte-jingai-musume-to-honobono-suru"
bw = "https://bookwalker.jp/series/187574/"

[chapters]
released = 40.2
read = 36
last_checked = 2021-09-04T00:00:00Z
+++

it's basically identical to LDM but better get owned quof