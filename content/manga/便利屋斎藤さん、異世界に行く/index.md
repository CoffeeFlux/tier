+++
title = "便利屋斎藤さん、異世界に行く"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=151039"
raw = "https://comic-walker.com/contents/detail/KDCW_EB00000027010000_68/"
md = "https://mangadex.org/title/31078/handyman-saitou-in-another-world"
bw = "https://bookwalker.jp/series/211520/"

[chapters]
released = 137
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

It's basically a 4koma but not in 4koma format; little more than someone's shower thoughts.