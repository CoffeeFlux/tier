+++
title = "史上最強の大魔王、村人Aに転生する"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155613"
raw = ""
md = "https://mangadex.org/title/14677fdb-7b5f-4178-9775-dc3e11752f0f/shijou-saikyou-no-daimaou-murabito-a-ni-tensei-suru"
bw = "https://bookwalker.jp/series/214648/"

[chapters]
released = 25
read = 15
last_checked = 2021-12-27T00:00:00Z
+++
