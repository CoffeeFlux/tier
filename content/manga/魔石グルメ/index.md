+++
title = "魔石グルメ"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
tags = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154663"
raw = "https://comic-walker.com/contents/detail/KDCW_FS00000019010000_68/"
md = "https://mangadex.org/title/38291/maseki-gurume-mamono-no-chikara-o-tabeta-ore-wa-saikyou"
bw = "https://bookwalker.jp/series/225253/"

[chapters]
released = 27
read = 25
last_checked = 2021-09-04T00:00:00Z
+++

The heroine is good. The side characters are good. The protag is decent. Very feelsgood manga. I enjoy it.