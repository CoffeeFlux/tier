+++
title = "Live Dungeon!"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["game", "isekai"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150480"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200320010000_68/"
md = "https://mangadex.org/title/24659/live-dungeon"
bw = "https://bookwalker.jp/series/171559/"


[chapters]
released = 42
read = 39
last_checked = 2021-09-04T00:00:00Z
+++

Imagine log horizon except exclusively focused on MMO mechanics. Including really good obligatory male friend character