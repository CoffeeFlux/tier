+++
title = "どうしても破滅したくない悪役令嬢が現代兵器を手にした結果がこれです"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153995"
raw = "https://pocket.shonenmagazine.com/episode/10834108156703419092"
md = "https://mangadex.org/title/0a53a3ba-fbbb-490e-9c6c-0daf7ccbc3df/the-villainess-will-crush-her-destruction-end-through-modern-firepower"
bw = "https://bookwalker.jp/series/235738"

[chapters]
released = 57
read = 57
last_checked = 2021-09-26T00:00:00Z
+++

It's basically a pixiv-tier manga with a target demographic of 2ch/4ch users. The early chapters more or less completely forgo backgrounds, the chapters are very short, releases biweekly, kind of hard to recommend. Most of the powerups given to the protag are just handwaved by the 'epic gnome' that just shows up randomly, helps her out, then disappears. Someone just wanted to write something where the protag is a loli with massive guns, so they wrote it.