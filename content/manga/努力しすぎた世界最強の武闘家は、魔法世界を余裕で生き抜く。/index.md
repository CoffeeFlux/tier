+++
title = "努力しすぎた世界最強の武闘家は、魔法世界を余裕で生き抜く。"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=151624"
raw = "http://seiga.nicovideo.jp/comic/35630"
md = "https://mangadex.org/title/29402/doryoku-shisugita-sekai-saikyou-no-butouka-ha-mahou-sekai-wo-yoyuu-de-ikinuku"
bw = "https://bookwalker.jp/series/179497/"

[chapters]
released = 63
read = 28
last_checked = 2021-09-04T00:00:00Z
+++