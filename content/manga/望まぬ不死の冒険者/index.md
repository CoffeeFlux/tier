+++
title = "望まぬ不死の冒険者"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=145794"
raw = "https://comic-gardo.com/episode/10834108156661710518"
md = "https://mangadex.org/title/22775/nozomanu-fushi-no-boukensha"
bw = "https://bookwalker.jp/series/161597/"

[chapters]
released = 38
read = 32
last_checked = 2021-09-04T00:00:00Z
+++