+++
title = "村人転生 最強のスローライフ"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mui = "https://www.mangaupdates.com/series.html?id=146903"
raw = "http://seiga.nicovideo.jp/comic/34130"
md = "https://mangadex.org/title/27211/murabito-tensei-saikyou-no-slow-life"
bw = "https://bookwalker.jp/series/148904/"

[chapters]
released = 40.1
read = 35
last_checked = 2021-09-04T00:00:00Z
+++

it's a slowlife manga that isn't slowlife.