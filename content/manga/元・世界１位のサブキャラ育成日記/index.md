+++
title = "元・世界１位のサブキャラ育成日記"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=151825"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01200682010000_68/"
md = "https://mangadex.org/title/34196/moto-sekai-ichi-i-subchara-ikusei-nikki-hai-player-isekai-wo-kouryakuchuu"
bw = "https://bookwalker.jp/series/220038/"

[chapters]
released = 30.1
read = 26
last_checked = 2021-09-04T00:00:00Z
+++