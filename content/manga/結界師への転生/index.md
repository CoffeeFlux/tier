+++
title = "結界師への転生"
statuses = ["ongoing"]
demographics = []
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150978"
raw = "https://comic-boost.com/series/116"
md = "https://mangadex.org/title/38377/kekkaishi-e-no-tensei"
bw = "https://bookwalker.jp/series/206397/"

[chapters]
released = 26.1
read = 22
last_checked = 2021-09-04T00:00:00Z
+++

Well at least it doesn't have skill spam. I'm not really sure what direction the author wants to take this story in though; It seems a little aimless.