+++
title = "死んでください！勇者でしょ？"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149701"
raw = "https://comic-walker.com/contents/detail/KDCW_AM06200502010000_68/"
md = "https://mangadex.org/title/ccb82067-e8e6-412e-83f4-2539b121bfc4/shinde-kudasai-yuusha-desho"
bw = "https://bookwalker.jp/series/196790/list"

[chapters]
released = 15
read = 15
last_checked = 2021-10-24T00:00:00Z
+++

It's a little too short but otherwise okay.