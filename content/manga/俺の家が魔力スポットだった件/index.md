+++
title = "俺の家が魔力スポットだった件"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149076"
raw = "http://seiga.nicovideo.jp/comic/34698"
md = "https://mangadex.org/title/27467/ore-no-ie-ga-maryoku-spot-datta-ken-sundeiru-dake-de-sekai-saikyou"
bw = "https://bookwalker.jp/series/171540/"

[chapters]
released = 138
read = 66
last_checked = 2021-09-04T00:00:00Z
+++

The first like 12 chapters are great. And then the author decides to flip the proverbial table, and just begins genre shifting the entire thing into oblivion. I can't really rec reading anything after ch12 or so but if you try to do it hopefully you enjoy it more than i did