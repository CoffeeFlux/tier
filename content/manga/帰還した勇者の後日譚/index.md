+++
title = "帰還した勇者の後日譚"
tags = []
categories = ["isekai", "post-isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=159827"
raw = "https://magazine.jp.square-enix.com/gfantasy/story/kikanshitayuusya/"
md = "https://mangadex.org/title/84f193ec-e731-4bd4-bcb5-09e85dca3ebc/the-fate-of-the-returned-hero"
bw = "https://bookwalker.jp/series/250754/"

[chapters]
released = 14
read = 14
last_checked = 2021-10-21T00:00:00Z
+++
