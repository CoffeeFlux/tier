+++
title = "絶対に働きたくないダンジョンマスターが惰眠をむさぼるまで"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149649"
raw = "https://comic-gardo.com/episode/10834108156661710098"
md = "https://mangadex.org/title/29336/lazy-dungeon-master/"
bw = "https://bookwalker.jp/series/195865/"

[chapters]
released = 33
read = 27
last_checked = 2021-09-04T00:00:00Z
+++

Irrational Sociopath Leg-Fetishist Protag slaughters bandits with reckless abandon while ""doing nothing"" all day. The early chapters go relatively deep into tower/base defense autism and then it just scales back and becomes relatively normal with no risk, and subsequently becomes rather boring. Might be worth it for the earlier bit although maybe just play a tower defense game instead of being forced to see lolis constantly pee themselves.