+++
title = "異世界魔法は遅れてる！"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=138265"
raw = "https://comic-gardo.com/episode/10834108156661711381"
md = "https://mangadex.org/title/20444/isekai-mahou-wa-okureteru"
bw = "https://bookwalker.jp/series/120431/"

[chapters]
released = 42
read = 23
last_checked = 2021-09-04T00:00:00Z
+++

rational protagonist OBLITERATES fantasy world with FACTS and LOGIC. there's a few good panels here and there but the writing is kind of garbage and there's a little too much angst. ultimately i like the art. why is the "heroine" wearing a sailor seifuku later on tho....