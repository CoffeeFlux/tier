+++
title = "Pandora Hearts"
statuses = ["completed"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = []
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=9709"
md = "https://mangadex.org/title/1057/pandora-hearts"
bw = "https://bookwalker.jp/series/2167/"

[chapters]
released = 104
read = 0
last_checked = 2021-09-04T00:00:00Z
+++