+++
title = "チート薬師のスローライフ"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=152496"
raw = "http://gammaplus.takeshobo.co.jp/manga/cheat_yakushi/"
md = "https://mangadex.org/title/33136/cheat-kusushi-no-slow-life-isekai-ni-tsukurou-drugstore"
bw = "https://bookwalker.jp/series/210332/"

[chapters]
released = 30.2
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

Do you see the wolfgirl loli on the cover picture? Because the series is basically 90% her speaking in broken fragments which is supposedly "moe" along with her general reactions so if you want to read that then go ahead. The author also throws in a ton of references to other (better) works.