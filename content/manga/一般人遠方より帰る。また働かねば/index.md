+++
title = "一般人遠方より帰る。また働かねば"
tags = []
categories = ["isekai", "reverse-isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169086"
raw = "https://pocket.shonenmagazine.com/episode/13933686331680553803"
md = "https://mangadex.org/title/d577918a-076d-4db7-9ce6-24a8c275a03e/ippanjin-enpou-yori-kaeru-mata-hatara-kaneba"
bw = "https://bookwalker.jp/series/280652/"

[chapters]
released = 14
read = 14
last_checked = 2022-01-27T00:00:00Z
+++

Went on hiatus before it ever really started, but I enjoyed what was there. I'm tempted to read the WN/LN of it at some point since I highly doubt this adaptaiton will ever receive another chapter.