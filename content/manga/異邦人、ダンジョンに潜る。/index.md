+++
title = "異邦人、ダンジョンに潜る。"
tags = []
demographics = ["shounen"]
statuses = ["axed"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156905"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04201257010000_68/"
md = "https://mangadex.org/title/41850/ihoujin-dungeon-ni-moguru"
bw = "https://bookwalker.jp/series/237223/"

[chapters]
released = 12
read = 12
last_checked = 2021-09-04T00:00:00Z
+++
