+++
title = "アルゲートオンライン"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143763"
raw = "https://www.alphapolis.co.jp/manga/official/230000179"
md = "https://mangadex.org/title/24e5f5b0-c14b-4597-bb0a-606c583a657c/argate-online"
bw = "https://bookwalker.jp/series/158352"

[chapters]
released = 42
read = 33
last_checked = 2021-09-27T00:00:00Z
+++

The character design is like something straight out of the 90s which is kind of cool.