+++
title = "さようなら竜生, こんにちは人生"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=129695"
raw = "https://www.alphapolis.co.jp/manga/official/333000127"
md = "https://mangadex.org/title/18991/sayounara-ryuusei-konnichiwa-jinsei"
bw = "https://bookwalker.jp/series/104425/"

[chapters]
released = 61
read = 0
last_checked = 2021-09-04T00:00:00Z
+++