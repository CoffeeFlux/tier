+++
title = "The Dungeon Seeker"
statuses = ["completed"]
demographics = ["shounen"]
sexual_contents = [""]
sources = []
furigana = []
categories = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=133843"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/445000143"
md = "https://mangadex.org/title/19141/dungeon-seeker"
bw = "https://bookwalker.jp/series/108464/"

[chapters]
released = 33
read = 33
last_checked = 2021-09-04T00:00:00Z
+++

Relatively short revenge manga that is nothing but edgy revenge.