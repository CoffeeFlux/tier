+++
title = "いつでも自宅に帰れる俺は、異世界で行商人をはじめました"
tags = []
categories = ["isekai"]
demographics = []
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=170598"
raw = "https://seiga.nicovideo.jp/comic/49186?track=rank"
md = "https://mangadex.org/title/b252e636-0143-42d7-903c-de0c9772d5fb/itsudemo-jitaku-ni-kaerareru-ore-wa-isekai-de-gyoushounin-o-hajimemashita"
bw = "https://bookwalker.jp/series/291752/"

[chapters]
released = 17
read = 16
last_checked = 2021-10-09T00:00:00Z
+++
