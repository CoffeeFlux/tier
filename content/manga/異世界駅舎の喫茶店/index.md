+++
title = "異世界駅舎の喫茶店"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=142230"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000034010000_68/"
md = "https://mangadex.org/title/2370da67-5ddd-4cdb-9ed6-0963c6b322cb/isekai-ekisha-no-kissaten"
bw = "https://bookwalker.jp/series/112873/"

[chapters]
released = 27
read = 27
last_checked = 2022-02-11T00:00:00Z
+++

Ultimately the problem with isekai cooking series is that the authors generally have no originality and just try to recreate japanese cuisine every time. "omg miso and soysauce and all of the other japanese staples uooooh" etc over and over again. One benefit of this series is that the tankobons have recipes after the chapters I suppose, but if you wanted recipes there's enough online you can browse for free. Some of the chapters are kind of cringe, like almost every chapter involving the perverted knight.