+++
title = "転生したら乙女ゲーの世界？ いえ、魔術を極めるのに忙しいのでそういうのは結構です。"
tags = []
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=157243"
raw = "https://seiga.nicovideo.jp/comic/43714"
md = "https://mangadex.org/title/d6203314-9b0a-44ab-8c94-1368a9891ef2/reincarnated-into-an-otome-game-nah-i-m-too-busy-mastering-magic"
bw = "https://bookwalker.jp/series/238274/"

[chapters]
released = 22.2
read = 11.2
last_checked = 2021-09-16T00:00:00Z
+++

it's cute