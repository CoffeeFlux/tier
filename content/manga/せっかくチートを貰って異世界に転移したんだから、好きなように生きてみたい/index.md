+++
title = "せっかくチートを貰って異世界に転移したんだから、好きなように生きてみたい"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147560"
raw = "http://comicride.jp/sekkache/"
md = "https://mangadex.org/title/d36b802a-7d1e-4153-80ee-b6ee509555ff/sekkaku-cheat"
bw = "https://bookwalker.jp/series/184399/"

[chapters]
released = 33
read = 20
last_checked = 2021-11-21T00:00:00Z
+++

It's like ishuzoku except with an isekai'd dude that has healing powers, which eventually turns into a mecha series with sex on the side? ch15 is based btw.