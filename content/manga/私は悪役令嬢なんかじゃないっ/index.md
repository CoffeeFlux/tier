+++
title = "私は悪役令嬢なんかじゃないっ"
tags = []
categories = []
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155082"
raw = "https://www.alphapolis.co.jp/manga/official/297000260"
md = "https://mangadex.org/title/773e5993-821a-4cea-8b44-f4450112a8f5/i-m-not-a-villainess-just-because-i-can-control-darkness-doesn-t-mean-i-m-a-bad-person"
bw = "https://bookwalker.jp/series/245472/"

[chapters]
released = 19
read = 13
last_checked = 2021-09-19T00:00:00Z
+++

extremely tonedeaf darkskin racism where the protagonist gets whitewashed, and then they throw away that chapter, redo it, and unwhitewash her. It has amusement factor for that alone but it's seriously hard to recommend.