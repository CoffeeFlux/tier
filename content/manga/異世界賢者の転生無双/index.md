+++
title = "異世界賢者の転生無双"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=162833"
raw = "https://www.ganganonline.com/contents/isekaikenjya/"
md = "https://mangadex.org/title/3d3dda69-7387-4732-b114-e4bb3de67180/isekai-kenja-no-tensei-musou-geemu-no-chishiki-de-isekai-saikyou"
bw = "https://bookwalker.jp/series/236589/"

[chapters]
released = 15
read = 15
last_checked = 2021-09-13T00:00:00Z
+++

I'm actually enjoying this a fair amount. A standard powerfantasy game reincarnation scenario where the protag dabs on the plebs with his superior mmo gamer knowledge