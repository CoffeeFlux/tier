+++
title = "魔王様、リトライ!"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146909"
raw = "https://futabanet.jp/list/monster/work/5dbfa19177656129f3010000"
md = "https://mangadex.org/title/ff2beee0-7ba8-4661-ad80-f5e85337b540/maou-sama-retry"
bw = "https://bookwalker.jp/series/157025"

[chapters]
released = 43
read = 35
last_checked = 2021-12-27T00:00:00Z
+++
