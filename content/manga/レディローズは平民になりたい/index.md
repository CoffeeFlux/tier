+++
title = "レディローズは平民になりたい"
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150007"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200360010000_68/"
md = "https://mangadex.org/title/29177/lady-rose-wants-to-be-a-commoner"
bw = "https://bookwalker.jp/series/183895/"

[chapters]
released = 28.5
read = 17
last_checked = 2021-09-04T00:00:00Z
+++