+++
title = "皇帝つき女官は花嫁として望まれ中"
statuses = ["ongoing"]
demographics = ["josei"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154873"
raw = "https://online.ichijinsha.co.jp/zerosum/comic/koute"
md = "https://mangadex.org/title/38702/the-emperor-s-court-lady-is-wanted-as-a-bride"
bw = "https://bookwalker.jp/series/224738/"

[chapters]
released = 22
read = 15
last_checked = 2021-09-04T00:00:00Z
+++

While it's nice that the protag is at least somewhat competent, constantly being told she shouldn't do anything so as to not hurt herself is rather annoying.