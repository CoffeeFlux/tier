+++
title = "回復術士のやり直し"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=143756"
raw = "https://web-ace.jp/youngaceup/contents/1000049"
md = "https://mangadex.org/title/21758/kaifuku-jutsushi-no-yarinaoshi"
bw = "https://bookwalker.jp/series/156159/"

[chapters]
released = 38.1
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

If you wanted to read a rape/revenge h-doujin but also wanted bad plot mixed in and also serialized for years.