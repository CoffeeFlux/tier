+++
title = "魔剣師の魔剣による魔剣のためのハーレムライフ"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153730"
raw = "http://gammaplus.takeshobo.co.jp/manga/makenshi_harem/"
md = "https://mangadex.org/title/36197/makenshi-no-maken-niyoru-maken-no-tame-no-harem-life"
bw = "https://bookwalker.jp/series/229285/"

[chapters]
released = 15
read = 15
last_checked = 2021-09-04T00:00:00Z
+++

sociopathic protag booba. Only real value is the sex scenes although you're probably better off just reading actual hdoujins.