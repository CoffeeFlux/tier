+++
title = "完全回避ヒーラーの軌跡"
statuses = ["ongoing"]
demographics = []
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154019"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000160010000_68/"
md = "https://mangadex.org/title/36773/kanzen-kaihi-healer-no-kiseki"
bw = "https://bookwalker.jp/series/223172/"

[chapters]
released = 25
read = 24
last_checked = 2021-09-04T00:00:00Z
+++

The extremely bizarre hatred towards evasion, along with the lack of the protag ever kind of clarifying to anybody that his healing skill is off the charts becomes annoying quickly.