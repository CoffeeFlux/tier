+++
title = "現実主義勇者の王国再建記"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=143762"
raw = "https://comic-gardo.com/episode/10834108156661710698"
md = "https://mangadex.org/title/21809/genjitsushugi-yuusha-no-oukoku-saikenki"
bw = "https://bookwalker.jp/series/151122/"

[chapters]
released = 39
read = 38
last_checked = 2021-09-04T00:00:00Z
+++

It's decent. There's quite a fair amount of military/king isekai stories out there that go deep into strategy and tactics, and this one more or less leans in that direction as well. The protagonist is not all-powerful, although he does have quite extensive knowledge about a wide array of topics that are Exactly what they need at the Exact right moment, to the point where it never really feels like the protag is ever in a pinch. It's an oretueee without the oretueee.