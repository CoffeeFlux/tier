+++
title = "ハズレ枠の【状態異常スキル】で最強になった俺がすべてを蹂躙するまで"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155369"
raw = "https://comic-gardo.com/episode/10834108156661738245"
md = "https://mangadex.org/title/39571/hazure-waku-no-joutai-ijou-skill-de-saikyou-ni-natta-ore-ga-subete-wo-juurin-suru-made"
bw = "https://bookwalker.jp/series/230059/"

[chapters]
released = 23
read = 21
last_checked = 2021-09-04T00:00:00Z
+++