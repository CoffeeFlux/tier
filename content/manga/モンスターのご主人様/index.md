+++
title = "モンスターのご主人様"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146606"
raw = "https://gaugau.futabanet.jp/list/work/5dce954877656110b1000000"
md = "https://mangadex.org/title/1aa09c5e-d6cd-4662-93f2-8c64331b6234/monster-no-goshujin-sama"
bw = "https://bookwalker.jp/series/152446"

[chapters]
released = 44.1
read = 39
last_checked = 2022-02-14T00:00:00Z
+++
