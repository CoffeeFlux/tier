+++
title = "魔王様の街づくり"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=147727"
raw = ""
md = "https://mangadex.org/title/8e8b281f-a931-45c2-93e8-e350b0ed58b0/maou-sama-no-machizukuri-saikyou-no-dungeon-wa-kindai-toshi"
bw = "https://bookwalker.jp/series/177197"

[chapters]
released = 0
read = 30
last_checked = 2021-09-17T00:00:00Z
+++
