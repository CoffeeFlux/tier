+++
title = "地味な剣聖はそれでも最強です"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=151025"
raw = "https://pash-up.jp/content/00000194"
md = "https://mangadex.org/title/31002/jimi-na-kensei-wa-sore-demo-saikyou-desu"
bw = "https://bookwalker.jp/series/197788/"

[chapters]
released = 59
read = 0
last_checked = 2021-09-04T00:00:00Z
+++

It's like one punch man but even more mediocre and without the good side characters and not really a parody