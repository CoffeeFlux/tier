+++
title = "冬フェンリルの愛子となった私が、絶望から癒されていく話"
tags = []
categories = ["isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=163243"
raw = "https://comic.k-manga.jp/title/122337/pv"
md = "https://mangadex.org/title/a5fe1bd9-3e02-40f9-be4f-8852b69cabf7/fuyu-fenrir-no-itoshigo-to-natta-watashi-ga-zetsubou-kara-iyasareteiku-hanashi"
bw = "https://bookwalker.jp/series/286131"

[chapters]
released = 17
read = 17
last_checked = 2022-01-17T00:00:00Z
+++

The protag is cute but the work is nothing more than oretueee of arbitrary magic that solves every problem and the magic of friendship on the side. First chapter is meme-level corporate suffering worth reading for that chapter alone. Would rec if you think the protag is cute but not otherwise.