+++
title = "転生したらドラゴンの卵だった"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=147652"
raw = "http://comic-earthstar.jp/detail/doratama/"
md = "https://mangadex.org/title/23774/tensei-shitara-dragon-no-tamago-datta-ibara-no-dragon-road"
bw = "https://bookwalker.jp/series/164419/"

[chapters]
released = 21
read = 20.7
last_checked = 2021-09-04T00:00:00Z
+++

This is fairly similar to 蜘蛛ですが, except the protag gets to interact with other people *far* quicker.