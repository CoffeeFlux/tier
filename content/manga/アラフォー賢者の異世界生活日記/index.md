+++
title = "アラフォー賢者の異世界生活日記"
tags = []
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149663"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000071010000_68/"
md = "https://mangadex.org/title/adce0aea-3f6d-4fd2-9902-1107f59f94da/arafoo-kenja-no-isekai-seikatsu-nikki"
bw = "https://bookwalker.jp/series/181859"

[chapters]
released = 31
read = 29
last_checked = 2022-01-19T00:00:00Z
+++
