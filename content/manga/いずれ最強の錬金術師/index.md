+++
title = "いずれ最強の錬金術師"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=167634"
raw = "https://www.alphapolis.co.jp/manga/official/443000228"
md = "https://mangadex.org/title/17b4a134-65a5-4ba9-851e-9cc824755bf5/someday-will-i-be-the-greatest-alchemist"
bw = "https://bookwalker.jp/series/197173"

[chapters]
released = 32
read = 10
last_checked = 2022-01-23T00:00:00Z
+++

Skilldumps? Water Pumps? Slavery? Evil church? Extremely broken powers? Random adventurers guild where you only have to do a quest occasionally to pay no toll to enter a city? Yep it's an isekai.