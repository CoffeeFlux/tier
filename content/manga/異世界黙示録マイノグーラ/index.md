+++
title = "異世界黙示録マイノグーラ"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170140"
raw = "https://comic-walker.com/contents/detail/KDCW_AM00000023010000_68/"
md = "https://mangadex.org/title/6e156d65-cf65-4d5b-9d04-52f2f8100fbf/isekai-apocalypse-mynoghra-the-conquest-of-the-world-starts-with-the-civilisation-of-ruin"
bw = "https://bookwalker.jp/series/264824"

[chapters]
released = 12.2
read = 11.1
last_checked = 2021-09-16T00:00:00Z
+++
