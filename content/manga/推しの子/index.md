+++
title = "推しの子"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = ["none"]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164920"
raw = "https://tonarinoyj.jp/episode/13933686331626273051"
md = "https://mangadex.org/title/296cbc31-af1a-4b5b-a34b-fee2b4cad542/oshi-no-ko"
bw = "https://bookwalker.jp/series/253803"

[chapters]
released = 73
read = 73
last_checked = 2022-03-11T00:00:00Z
+++

The beginning of the story has these cryptic "flashback" pages at the start of every chapter that allude to something more with the story or flesh out the world more which i'm rather fond of. Later on the story develops into a relatively standard teenage Drama story, with a large focus on show business and romantic relationships; I'd say a good amount of the work is trying to convey hardships that working artists go through regardless of medium they're involved in, and possibly some of the drama/hardship that arises from their daily life.