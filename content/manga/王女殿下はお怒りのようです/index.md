+++
title = "王女殿下はお怒りのようです"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["light_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155606"
raw = "https://comic-gardo.com/episode/10834108156673596521"
md = "https://mangadex.org/title/efa1e330-e318-4669-b9a7-878ac2618cab/her-royal-highness-seems-to-be-angry"
bw = "https://bookwalker.jp/series/236821"

[chapters]
released = 20
read = 18
last_checked = 2021-09-27T00:00:00Z
+++

It's kind of interesting that this is something like shounen with a josei plot, blending the two together to make something kino.