+++
title = "異世界でも無難に生きたい症候群"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152746"
raw = "https://magcomi.com/episode/10834108156766386086"
md = "https://mangadex.org/title/6a769300-091e-49ab-8cd3-cc32c360ecc0/isekai-demo-bunan-ni-ikitai-shoukougun"
bw = "https://bookwalker.jp/series/217685"

[chapters]
released = 25
read = 23
last_checked = 2021-10-23T00:00:00Z
+++

lawful neutral sociopathic INTJ protag defeats evil with logic and rationality, while everybody else around him is excessively powerful.