+++
title = "理想のヒモ生活"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=139230"
raw = "https://comic-walker.com/contents/detail/KDCW_KS02000048010000_68/"
md = "https://mangadex.org/title/fac7bdc7-c1f3-4595-82f5-b5bfb002b933/risou-no-himo-seikatsu"
bw = "https://bookwalker.jp/series/119694"

[chapters]
released = 53
read = 40
last_checked = 2021-10-09T00:00:00Z
+++
