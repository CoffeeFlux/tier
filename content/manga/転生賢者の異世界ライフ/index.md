+++
title = "転生賢者の異世界ライフ"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150779"
raw = "http://www.ganganonline.com/contents/tensei/"
md = "https://mangadex.org/title/30744/tensei-kenja-no-isekai-life-daini-no-shokugyou-wo-ete-sekai-saikyou-ni-narimashita"
bw = "https://bookwalker.jp/series/172921/"

[chapters]
released = 37.2
read = 27
last_checked = 2021-09-04T00:00:00Z
+++

It's kind of amusing(?) that the protag's first thought is to kill himself. I'd say this is more of a comedy than anything, but the protag kind of ruins a lot of the comedy by being too much of straightman. A little too much skill autism as well. The tamed monsters are amusing enough though. coders rise up.