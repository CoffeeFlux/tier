+++
title = "女王陛下の異世界戦略"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=171386"
raw = "https://seiga.nicovideo.jp/comic/48555"
md = "https://mangadex.org/title/559be949-4475-4f06-b8bf-a1a7343489bc/her-majesty-s-swarm"
bw = "https://bookwalker.jp/series/287303"

[chapters]
released = 23
read = 23
last_checked = 2022-01-25T00:00:00Z
+++

tldr strategy game enthusiast girl get's isekai'd as the queen of the race that she always played in the game that she always played; the race is the chaotic evil spiders; she slowly gets mind corrupted over time and everybody dies gruesomely/memely. the end.