+++
title = "ボクラノキセキ"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=34396"
raw = ""
md = "https://mangadex.org/title/fdb3ae24-7e0d-4fa9-94e2-b194ee4d8217/bokura-no-kiseki"
bw = "https://bookwalker.jp/series/4059"

[chapters]
released = 115
read = 113
last_checked = 2021-01-10T00:00:00Z
+++

Kind of a weird series that is almost exclusively told in flashbacks where every character is an unreliable narrator. It becomes a little too melodramatic at times and any time an 'enemy' mostly gets resolved another one pops up randomly out of the woodwork to continue to string the story along. This series has been ongoing for ages and while it feels like it's finally nearing the end, it's felt like that for ages, so it might be the onepiece of shoujo. All that being said, I do enjoy it a good fair bit and feel like most other people would enjoy it as well as it tries to weave a very large story of politicial intrique that the current people can do nothing about yet are still affected by indirectly, the emotions that come about from remembering all of that, and more.