+++
title = "狼は眠らない"
tags = []
categories = []
demographics = []
statuses = []
furigana = []
sexual_contents = []
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154268"
raw = "https://www.kadokawa.co.jp/product/321810000560/"
md = "https://mangadex.org/title/9038211c-65b3-43b8-9471-9bda65049220/ookami-wa-nemuranai"
bw = "https://bookwalker.jp/series/213111"

[chapters]
released = 0
read = 0
last_checked = 1970-01-01T00:00:00Z
+++
