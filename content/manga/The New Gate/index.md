+++
title = "The New Gate"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=116976"
raw = "https://www.alphapolis.co.jp/manga/official/428000082"
md = "https://mangadex.org/title/b41bef1e-7df9-4255-bd82-ecf570fec566/the-new-gate"
bw = "https://bookwalker.jp/series/83899"

[chapters]
released = 73
read = 72
last_checked = 2021-10-01T00:00:00Z
+++
