+++
title = "最強の魔物になる道を辿る俺、異世界中でざまぁを執行する"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=187801"
raw = "https://www.yomonga.com/title/1582"
md = "https://mangadex.org/title/1e8d04cf-400c-4ffb-af6b-108d851f1b7d/saikyou-no-mamono-ni-narumichi-wo-tadoru-ore-isekaijuu-de-zamaa-wo-shikkou-suru"
bw = "https://bookwalker.jp/dec63d9ac8-75de-4c4f-9563-7bda1c62d1ac/"

[chapters]
released = 0
read = 12
last_checked = 2022-01-21T00:00:00Z
+++

I never thought i'd come across an edgy isekai manga that makes kaiyari man's writing seem good but here we are.