+++
title = "Re:Monster"
statuses = ["ongoing"]
demographics = ["seinen"]
sexual_contents = [""]
sources = []
furigana = []
categories = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=109745"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/609000058"
md = "https://mangadex.org/title/12171/re-monster"
bw = "https://bookwalker.jp/series/62182/"

[chapters]
released = 73
read = 67
last_checked = 2021-09-04T00:00:00Z
+++