+++
title = "転生領主の優良開拓"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=191926"
raw = ""
md = "https://mangadex.org/title/1b9181b3-98d7-4f60-8b36-66abe86f4204/tensei-ryoushu-no-yuuryou-kaitaku-zensei-no-kioku-o-ikashite-white-ni-tsutometara-yuunou-na-jinzai"
bw = "https://bookwalker.jp/series/275441/list/"

[chapters]
released = 16.2
read = 13
last_checked = 2022-02-13T00:00:00Z
+++

Dude worked at a black company and then gets reincarnated as a lord of a region but his parents die in a demon attack so he ends up all alone so he sends out job recruitment flyers that seem too good to be true so tons of people start showing up.