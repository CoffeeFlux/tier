+++
title = "復讐を希う最強勇者は、闇の力で殲滅無双する"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=152659"
raw = "https://seiga.nicovideo.jp/comic/39335"
md = "https://mangadex.org/title/34404/fukushuu-o-koinegau-saikyou-yuusha-wa-yami-no-chikara-de-senmetsu-musou-suru"
bw = "https://bookwalker.jp/series/218554/"

[chapters]
released = 39
read = 29
last_checked = 2021-09-04T00:00:00Z
+++

A cute twinbraid girl appears some ways into the story and then leaves the story never to return again. Absolutely tragic.