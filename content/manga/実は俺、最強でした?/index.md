+++
title = "実は俺、最強でした?"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153516"
raw = "http://seiga.nicovideo.jp/comic/40386"
md = "https://mangadex.org/title/9484b1fd-0271-4c9b-b096-7e313823058e/jitsu-wa-ore-saikyou-deshita"
bw = "https://bookwalker.jp/series/222358"

[chapters]
released = 39
read = 12
last_checked = 2021-09-19T00:00:00Z
+++
