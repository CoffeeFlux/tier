+++
title = "ゲート 自衛隊彼の地にて、斯く戦えり"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=72210"
raw = "https://www.alphapolis.co.jp/manga/official/138000030"
md = "https://mangadex.org/title/5353/gate-jieitai-kanochi-nite-kaku-tatakaeri"
bw = "https://bookwalker.jp/series/58696/"

[chapters]
released = 106
read = 0
last_checked = 2021-09-04T00:00:00Z
+++