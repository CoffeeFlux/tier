+++
title = "人間牧場"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=155278"
raw = "https://gammaplus.takeshobo.co.jp/manga/human_ranch/"
md = "https://mangadex.org/title/a31c3700-7a93-4b64-bfa4-9b09bf3a29dd/human-ranch"
bw = "https://bookwalker.jp/series/205197"

[chapters]
released = 23
read = 22
last_checked = 2021-12-26T00:00:00Z
+++

Isekai horror is kind of a weird niche tbh. Seems similar to Promised Neverland but it doesn't follow annoying children and also involves tons of random sex/gore scenes. I feel like if you want a gore-y horror it's still worth trying out but if you don't want that then it seems easy to pass on it.