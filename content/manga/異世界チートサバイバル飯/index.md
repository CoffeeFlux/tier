+++
title = "異世界チートサバイバル飯"
categories = ["isekai"]
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150722"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000081010000_68/"
md = "https://mangadex.org/title/35199/isekai-cheat-survival-meshi"
bw = "https://bookwalker.jp/series/193346/"

[chapters]
released = 35
read = 22
last_checked = 2021-09-04T00:00:00Z
+++