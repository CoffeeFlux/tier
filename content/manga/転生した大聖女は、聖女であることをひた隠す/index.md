+++
title = "転生した大聖女は、聖女であることをひた隠す"
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=159849"
raw = "https://www.comic-earthstar.jp/sp/detail/daiseijo/"
md = "https://mangadex.org/title/50692/the-reincarnated-great-saint-hides-that-she-s-a-saint"
bw = "https://bookwalker.jp/series/243878/"

[chapters]
released = 19
read = 14
last_checked = 2021-09-04T00:00:00Z
+++