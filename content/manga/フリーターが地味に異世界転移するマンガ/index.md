+++
title = "フリーターが地味に異世界転移するマンガ"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154017"
raw = "https://comic.pixiv.net/works/5613"
md = "https://mangadex.org/title/005143c6-d844-4e51-bd68-625cd66242c1/freeter-ga-jimini-isekai-teni-suru-manga"
bw = "https://bookwalker.jp/series/198238"

[chapters]
released = 18
read = 18
last_checked = 2022-02-01T00:00:00Z
+++

I thought this would be more interesting based on the synopsis, but it was kind of just whatever; The chapters are too short and it's basically just a 4koma. Tldr dude slips in and out of an isekai at random and does things or does nothing and then he slips back. the end.