+++
title = "異世界で手に入れた生産スキルは最強だったようです。"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=163161"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000280010000_68/"
md = "https://mangadex.org/title/d68ceffd-ac56-45db-9129-3413dd0d7063/isekai-de-te-ni-ireta-seisan-skill-wa-saikyou-datta-you-desu"
bw = "https://bookwalker.jp/series/255881"

[chapters]
released = 16
read = 11
last_checked = 2021-09-30T00:00:00Z
+++
