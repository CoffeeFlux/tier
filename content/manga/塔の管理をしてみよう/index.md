+++
title = "塔の管理をしてみよう"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=148886"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000064010000_68/"
md = "https://mangadex.org/title/27005/tou-no-kanri-o-shite-miyou"
bw = "https://bookwalker.jp/series/167399/"

[chapters]
released = 42
read = 0
last_checked = 2021-09-04T00:00:00Z
+++
