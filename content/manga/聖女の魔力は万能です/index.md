+++
title = "聖女の魔力は万能です"
tags = []
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146149"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00000002010000_68/"
md = "https://mangadex.org/title/23631/seijo-no-maryoku-wa-bannou-desu"
bw = "https://bookwalker.jp/series/148180/"

[chapters]
released = 28.4
read = 25.5
last_checked = 2021-09-04T00:00:00Z
+++

The glasses are good why did she have to take them off reeee. shoutout to ch19.5. It's a fairly moe series honestly; standard shoujo. The perspective of the other saint is appreciated since you can really feel how she was just swept along in bewilderment at all of the events. Overall it's just a really bogstandard shoujo but not bad at anything.