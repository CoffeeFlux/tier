+++
title = "貧乏令嬢の勘違い聖女伝"
tags = []
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=176561"
raw = "https://online.ichijinsha.co.jp/zerosum/comic/bimboreijo"
md = "https://mangadex.org/title/c7a10d9f-6628-4d58-b27a-5081ec3db27b/binbou-reijou-no-kanchigai-seijo-den"
bw = "https://bookwalker.jp/series/302587/"

[chapters]
released = 10
read = 10
last_checked = 2022-01-18T00:00:00Z
+++

Standard reincarnation loop where they get to re-do their life over from scratch and learn from their mistakes.