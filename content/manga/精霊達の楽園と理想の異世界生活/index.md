+++
title = "精霊達の楽園と理想の異世界生活"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149689"
raw = "https://comic-boost.com/series/101"
md = "https://mangadex.org/title/29595/seirei-tachi-no-rakuen-to-risou-no-isekai-seikatsu"
bw = "https://bookwalker.jp/series/175134/"

[chapters]
released = 36
read = 20
last_checked = 2021-09-04T00:00:00Z
+++

It's basically a town-building simulator but without the town, where the protag cheats his way to cultivating a barren wasteland. Actually thinking about it some more, it's Rune Factory.