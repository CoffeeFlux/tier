+++
title = "ネクストライフ"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=143159"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01000062010000_68/"
md = "https://mangadex.org/title/21604/next-life"
bw = "https://bookwalker.jp/series/137924/"

[chapters]
released = 42.1
read = 30
last_checked = 2021-09-04T00:00:00Z
+++