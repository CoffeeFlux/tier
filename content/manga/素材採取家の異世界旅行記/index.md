+++
title = "素材採取家の異世界旅行記"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146133"
raw = "https://www.alphapolis.co.jp/manga/official/220000195"
md = "https://mangadex.org/title/77c8cc57-95c1-4f9b-8843-493eb2697566/souzai-saishuka-no-isekai-ryokouki"
bw = "https://bookwalker.jp/series/184174"

[chapters]
released = 34
read = 33
last_checked = 2021-10-02T00:00:00Z
+++
