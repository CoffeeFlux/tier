+++
title = "双穹の支配者"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=163621"
raw = "https://seiga.nicovideo.jp/comic/48100"
md = "https://mangadex.org/title/49426/soukyuu-no-shihai-sha-isekai-oppai-musouden"
bw = "https://bookwalker.jp/series/275618/"

[chapters]
released = 14
read = 0
last_checked = 2021-09-04T00:00:00Z
+++
