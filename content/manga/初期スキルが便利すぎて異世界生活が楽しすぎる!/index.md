+++
title = "初期スキルが便利すぎて異世界生活が楽しすぎる!"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=172774"
raw = "https://www.alphapolis.co.jp/manga/official/469000311"
md = "https://mangadex.org/title/5127bc3e-6b87-4fa2-82eb-fd363cd4c10a/shoki-skill-ga-benri-sugite-isekai-seikatsu-ga-tanoshisugiru"
bw = "https://bookwalker.jp/series/261919"

[chapters]
released = 11
read = 11
last_checked = 2021-10-25T00:00:00Z
+++
