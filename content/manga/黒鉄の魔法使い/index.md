+++
title = "黒鉄の魔法使い"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sexual_contents = ["ecchi"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156904"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04201413010000_68/"
md = "https://mangadex.org/title/f97150c3-49a9-44ef-bef7-6973ae6e0462/kurogane-no-mahoutsukai"
bw = "https://bookwalker.jp/series/245745/"

[chapters]
released = 23
read = 23
last_checked = 2022-03-10T00:00:00Z
+++

The heroine is a cute psychopath and is probably the main highlight of this manga. Some of the angles are kind of weird since all of the non-heroine girls seemingly always wear a skirt when they fight, so they get upskirted a lot, and it's almost like they're not wearing any underwear for some reason; kind of cringe. Also main heroine's hair is just way too long for any of the combat she is doing. Also the manga just kind of got axed and ended abruptly without really tying any of its plot threads together which is a little disappointing.

TLDR: I cast fist.