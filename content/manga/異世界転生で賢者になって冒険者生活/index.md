+++
title = "異世界転生で賢者になって冒険者生活"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=175909"
raw = "https://www.ganganonline.com/title/1204"
md = "https://mangadex.org/title/258d2691-ffea-4e77-8968-ce5c24ffc6f7/isekai-tensei-de-kenja-ni-natte-boukensha-seikatsu"
bw = "https://bookwalker.jp/series/295787/"

[chapters]
released = 9
read = 9
last_checked = 2021-12-26T00:00:00Z
+++
