+++
title = "転生王子はダラけたい"
statuses = ["axed"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146624"
raw = "https://www.alphapolis.co.jp/manga/official/564000194"
md = "https://mangadex.org/title/25641/tensei-ouji-wa-daraketai"
bw = "https://bookwalker.jp/series/187288/"

[chapters]
released = 26
read = 26
last_checked = 2021-09-04T00:00:00Z
+++

A rather simple yet pleasant manga. It's not particularly outstanding but at least it doesn't have dumb skill spam. It's kind of unfortunate that it got 'axed', but the plot seemed to be on a downward trend either way.