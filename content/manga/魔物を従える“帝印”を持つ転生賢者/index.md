+++
title = "魔物を従える“帝印”を持つ転生賢者"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=165484"
raw = "https://magazine.jp.square-enix.com/joker/series/teiin/"
md = "https://mangadex.org/title/339a6d49-9cc3-4435-98b6-59ddec3a6c0d/mamono-wo-shitagaeru-teiin-wo-motsu-tensei-kenjya-katsute-no-maho-to-jyuma-de-hissori-saikyo-no-bokensha-ninaru"
bw = "https://bookwalker.jp/series/243989/"

[chapters]
released = 11
read = 6.1
last_checked = 2021-09-25T00:00:00Z
+++
