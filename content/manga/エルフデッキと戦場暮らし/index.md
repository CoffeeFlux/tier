+++
title = "エルフデッキと戦場暮らし"
tags = []
categories = ["reincarnation", "isekai", "game"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sexual_contents = ["nudity", "suggestive"]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=159787"
raw = "https://seiga.nicovideo.jp/comic/45010"
md = "https://mangadex.org/title/e7acdd83-e594-46ee-84e6-4d3b3fee4115/elf-deck-to-senjou-gurashi"
bw = "https://bookwalker.jp/series/230772"

[chapters]
released = 34
read = 34
last_checked = 1970-01-01T00:00:00Z
+++

It's a series based around magic the gathering, and it's basically a card game manga except isekai'd into the world of a card game while also following the rules of the game while also not. 

The work is somewhat rushed and kind of just ends but it does have a full "ending" so it's probably fine to keep it under the "completed" status. As far as i'm aware this is manga-original but maybe there's a WN out there somewhere. I feel like it could've done a lot more with the formula but maybe the author got bored and wanted to wrap it up.