+++
title = "奴隷転生"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=173301"
raw = "https://pocket.shonenmagazine.com/episode/13933686331722280688"
md = "https://mangadex.org/title/56584/dorei-tensei-sono-dorei-saikyou-no-moto-ouji-ni-tsuki"
bw = "https://bookwalker.jp/series/295378/"

[chapters]
released = 46
read = 40
last_checked = 2021-09-04T00:00:00Z
+++

I feel like it's building up to something cool, but i don't know how many years it would take to reach that point; maybe i'd go and read the novel at some point to see what it does but otherwise eh.