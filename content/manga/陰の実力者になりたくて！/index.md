+++
title = "陰の実力者になりたくて！"
statuses = ["ongoing"]
demographics = ["shounen"]
tags = []
categories = ["isekai", "reincarnation"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152254"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04200967010000_68/"
md = "https://mangadex.org/title/33085/kage-no-jitsuryokusha-ni-naritakute"
bw = "https://bookwalker.jp/series/212232/"

[chapters]
released = 32
read = 26
last_checked = 2021-09-04T00:00:00Z
+++

If the style of comedy works for you, then this is 神. Otherwise it might be a bad rec. I do feel like most people would enjoy it though.