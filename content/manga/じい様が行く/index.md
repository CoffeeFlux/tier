+++
title = "じい様が行く"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150762"
raw = "https://www.alphapolis.co.jp/manga/official/514000219"
md = "https://mangadex.org/title/f7bd1566-5b2e-4ffa-8895-ee61884cda98/jiisama-ga-iku"
bw = "https://bookwalker.jp/series/177692"

[chapters]
released = 44
read = 11
last_checked = 2022-01-29T00:00:00Z
+++

Fairly standard 'healing' manga where the main highlight is the good-natured interactions and gratefulness of most of the people that the protag interacts with. also a very cute twinbraid+glasses elf randomly pops in and out of the story constantly which is good, but also it's kind of weird when she shows up and then just immediately disappears without like saying goodbye.