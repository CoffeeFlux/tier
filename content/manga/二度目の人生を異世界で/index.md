+++
title = "二度目の人生を異世界で"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=137168"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000021010000_68/"
md = "https://mangadex.org/title/20163/nidome-no-jinsei-wo-isekai-de"
bw = "https://bookwalker.jp/series/90838/"

[chapters]
released = 49.3
read = 49.3
last_checked = 2021-09-04T00:00:00Z
+++

this author really be out here giving a character the name "femme fatale". Aside from that it's pretty good; gets excessively more based as time goes on. Kind of just ends though.