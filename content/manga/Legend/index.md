+++
title = "Legend"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=124977"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01000027010000_68/"
md = "https://mangadex.org/title/5f73422e-e47c-41a1-8b5d-96ba13498cfe/legend"
bw = "https://bookwalker.jp/series/75189/"

[chapters]
released = 66
read = 48
last_checked = 2021-10-02T00:00:00Z
+++

Early chapters are kind of bad but it eventually kind of balances out at some point