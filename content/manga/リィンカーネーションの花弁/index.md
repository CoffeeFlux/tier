+++
title = "リィンカーネーションの花弁"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=110861"
raw = "https://magcomi.com/episode/10834108156766291292"
md = "https://mangadex.org/title/3b5c0def-79ae-44e8-9d71-9aa2e83f9a9f/reincarnation-no-kaben"
bw = "https://bookwalker.jp/series/23929/list"

[chapters]
released = 76
read = 58
last_checked = 2021-09-25T00:00:00Z
+++

Although reincarnation is in the title it's fairly different from the other works on this list, since it is 'reverse-reincarnation'. It's a pretty standard 'dark-shounen' that has been releasing monthly for a very long time now. A decent amount of works have also done the whole 'influential people from the past fight each other because reasons' plotline but this work doesn't do a bad job of it so it's a solid rec if you want yet another take on it.