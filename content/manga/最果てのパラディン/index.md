+++
title = "最果てのパラディン"
statuses = ["ongoing"]
demographics = ["shounen"]
tags = []
categories = ["reincarnation"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144449"
raw = "https://comic-gardo.com/episode/10834108156661710626"
md = "https://mangadex.org/title/24651/saihate-no-paladin"
bw = "https://bookwalker.jp/series/156133/"

[chapters]
released = 38
read = 31
last_checked = 2021-09-04T00:00:00Z
+++