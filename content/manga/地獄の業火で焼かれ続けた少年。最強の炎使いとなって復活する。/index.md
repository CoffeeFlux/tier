+++
title = "地獄の業火で焼かれ続けた少年。最強の炎使いとなって復活する。"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169085"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666633377"
md = "https://mangadex.org/title/3edb0966-0a32-4afc-8107-637d03c26d3a/jigoku-no-gouka-de-yaka-re-tsuzuketa-shounen-saikyou-no-honou-tsukai-to-natte-fukkatsu-suru"
bw = "https://bookwalker.jp/series/275655"

[chapters]
released = 61
read = 19
last_checked = 2021-09-25T00:00:00Z
+++
