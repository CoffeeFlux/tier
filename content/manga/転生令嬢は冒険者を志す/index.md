+++
title = "転生令嬢は冒険者を志す"
tags = []
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156746"
raw = "http://seiga.nicovideo.jp/comic/40129"
md = "https://mangadex.org/title/e8b6d229-8feb-4994-ac77-9dd3af12aa6a/the-reincarnated-young-lady-aims-to-be-an-adventurer"
bw = "https://bookwalker.jp/series/235284"

[chapters]
released = 0
read = 0
last_checked = 2021-09-21T00:00:00Z
+++
