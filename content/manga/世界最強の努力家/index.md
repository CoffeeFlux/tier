+++
title = "世界最強の努力家"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=173430"
raw = "https://www.comic-earthstar.jp/detail/doryokuka/"
md = "https://mangadex.org/title/bfe0ab22-61fd-4ba1-8667-40c516c2dddd/sekai-saikyou-no-doryokuka-sainou-ga-doryoku-datta-no-de-kouritsu-yoku-kikakugai-no-doryoku-o"
bw = "https://bookwalker.jp/series/287957/list/"

[chapters]
released = 20
read = 19
last_checked = 2022-02-08T00:00:00Z
+++

The inclusion of this work in this list is a minor spoiler in itself since you won't really understand why for a while but i'd say it's worth reading since it's cool. It's similar to other works that focus on the Meta behind gaining random skills, the sociology that comes about due to it, and the innate properties of Humans. The art can be a little bad at times but it's tolerable. I don't know if it fully adapts the source material or if there's stuff left after it but the manga itself is largely self-contained with an "ending".