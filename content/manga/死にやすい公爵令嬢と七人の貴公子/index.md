+++
title = "死にやすい公爵令嬢と七人の貴公子"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169129"
raw = "https://gaugau.futabanet.jp/list/work/5dd4fe3077656139da070000"
md = "https://mangadex.org/title/30b68b83-d8cd-46c2-ad6c-e2eca355ab36/deathbound-duke-s-daughter-and-seven-noblemen"
bw = "https://bookwalker.jp/series/291304"

[chapters]
released = 15
read = 13
last_checked = 2022-01-23T00:00:00Z
+++

The protag is competent without being overly broken OP and the interactions thus far with all of the characters are nice. Chapters start to get really short after a little bit for whatever reason but maybe they'll pick up in size again later?