+++
title = "軍オタが魔法世界に転生したら、現代兵器で軍隊ハーレムを作っちゃいました？！"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=123074"
md = "https://mangadex.org/title/15946/gun-ota-ga-mahou-sekai-ni-tensei-shitara-gendai-heiki-de-guntai-harem-wo-tsukucchaimashita"
bw = "https://bookwalker.jp/series/61527/"

[chapters]
released = 69
read = 46
last_checked = 2021-09-04T00:00:00Z
+++