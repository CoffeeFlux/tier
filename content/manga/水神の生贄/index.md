+++
title = "水神の生贄"
statuses = ["completed"]
demographics = ["shoujo"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=119023"
md = "https://mangadex.org/title/15248/suijin-no-hanayome"
bw = "https://bookwalker.jp/series/127827/"

[chapters]
released = 44.5
read = 0
last_checked = 2021-09-04T00:00:00Z
+++