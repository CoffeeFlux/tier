+++
title = "死に戻り、全てを救うために最強へと至る"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
tags = []
categories = ["reincarnation"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156931"
raw = "https://urasunday.com/title/874"
md = "https://mangadex.org/title/41744/shi-ni-modori-subete-wo-sukuu-tame-ni-saikyou-he-to-itaru"
bw = "https://bookwalker.jp/series/237038/"

[chapters]
released = 38
read = 38
last_checked = 2021-09-04T00:00:00Z
+++