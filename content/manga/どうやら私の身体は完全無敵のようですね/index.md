+++
title = "どうやら私の身体は完全無敵のようですね"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149486"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200196010000_68"
md = "https://mangadex.org/title/29166/douyara-watashi-no-karada-wa-kanzen-muteki-no-you-desu-ne"
bw = "https://bookwalker.jp/series/168951/"

[chapters]
released = 43
read = 41
last_checked = 2021-09-04T00:00:00Z
+++

has same energy has noukin with an overpowered whitehair protagonist that doesn't want to stand out but can't help but standing out, with slight yuri vibes. very wareyacore but i'm not sure if i'd rec it to other people