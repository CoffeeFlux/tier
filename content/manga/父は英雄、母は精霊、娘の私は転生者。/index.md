+++
title = "父は英雄、母は精霊、娘の私は転生者。"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150727"
raw = "https://comic.pixiv.net/works/5551"
md = "https://mangadex.org/title/36246/dad-is-a-hero-mom-is-a-spirit-i-m-a-reincarnator"
bw = "https://bookwalker.jp/series/203951/"

[chapters]
released = 34
read = 34
last_checked = 2021-09-04T00:00:00Z
+++