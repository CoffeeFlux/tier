+++
title = "元獣医の令嬢は婚約破棄されましたが、もふもふたちに大人気です！"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=172933"
raw = "https://www.alphapolis.co.jp/manga/official/408000357"
md = "https://mangadex.org/title/d2a61f87-c74f-4ba3-93a4-c00a226dc02f/the-former-veterinarian-turned-duke-s-daughter-has-had-her-engagement-broken-off-but-she-s-still-very-popular-with-fluffy-beings"
bw = "https://bookwalker.jp/series/297184"

[chapters]
released = 12
read = 11
last_checed = 2021-12-25T00:00:00Z
+++

Excessively petty drama, comically evil antagonists, and disney-princess powers to overcome every situation. If this sounds amazing to you then go ahead and read it.