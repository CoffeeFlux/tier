+++
title = "異世界宗教へようこそ"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=158292"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02200709010000_68/"
md = "https://mangadex.org/title/60c8e9c1-d33c-4264-b825-07b99f035276/welcome-to-religion-in-another-world"
bw = "https://bookwalker.jp/series/227242/"

[chapters]
released = 18
read = 15
last_checked = 2022-02-08T00:00:00Z
+++

Denpa isekai about religion/cults that's largely focused on comedy and is kind of trippy at times.