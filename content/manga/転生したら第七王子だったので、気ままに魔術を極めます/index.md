+++
title = "転生したら第七王子だったので、気ままに魔術を極めます"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169082"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666634438"
md = "https://mangadex.org/title/b5c415a8-2af4-44cd-8b2e-edb23800e2c8/tensei-shitara-dai-nana-ouji-dattanode-kimamani-majutsu-o-kiwamemasu"
bw = "https://bookwalker.jp/series/275654"

[chapters]
released = 82
read = 71
last_checked = 2021-09-21T00:00:00Z
+++

I'll say now that if you don't like battle shounen you will not like this. But holy fuck how does a series that release weekly look this great. The usage of color is amazing, the overall panelling in fights is amazing, fights are extremely easy to follow along with without ever getting lost, etc. This is kino. It's at the point where I don't even think anime would be able to do the fights justice, which is odd considering that anime is typically where battle shounen shine the most; that's how well done this adaptation is. There's a few issues with the weird feminine-ish sexualization of the protag, and it leans a little too hard into 'comedy' at times with the meme faces for the protag but that can be charming in it's own ways.