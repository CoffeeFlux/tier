+++
title = "聖女じゃなかったので、王宮でのんびりご飯を作ることにしました"
tags = []
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=159241"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00201320010000_68/"
md = "https://mangadex.org/title/fd9caf16-9b4b-452b-b753-3d9bd1269fe4/i-m-not-the-saint-so-i-ll-just-leisurely-make-food-at-the-royal-palace"
bw = "https://bookwalker.jp/series/238275/"

[chapters]
released = 18.4
read = 14
last_checked = 2022-02-21T00:00:00Z
+++

A little more enjoyable than the standard cooking manga since it doesn't go all gungho on traditional japanese cooking and their fucking boner over soy sauce at least. The characters don't really feel like people at all though and just exist to fawn over the protag's Superior Otherwordly Cuisine while occasional fat jokes get thrown around haha lol.