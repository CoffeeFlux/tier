+++
title = "魔王様に召喚されたけど言葉が通じない。"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=159148"
raw = "https://comic-meteor.jp/maotujinai/"
md = "https://mangadex.org/title/bcab1d1d-029f-439b-893e-6e8553b007ba/i-was-summoned-by-the-demon-lord-but-i-can-t-understand-her-language"
bw = "https://bookwalker.jp/series/263792"

[chapters]
released = 24
read = 24
last_checked = 2021-10-03T00:00:00Z
+++

It's a moe romance work that goes all in on the language barrier.