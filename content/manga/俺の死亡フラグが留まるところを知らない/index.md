+++
title = "俺の死亡フラグが留まるところを知らない"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=168441"
raw = "https://www.mangabox.me/reader/127460/episodes/"
md = "https://mangadex.org/title/50011/my-death-flags-show-no-sign-of-ending"
az = "https://www.amazon.co.jp/dp/B08JGD36DQ"

[chapters]
released = 39
read = 39
last_checked = 2021-09-04T00:00:00Z
+++