+++
title = "孤児院テイマー"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156001"
raw = "https://firecross.jp/ebook/series/347"
md = "https://mangadex.org/title/05c2917a-e0a4-4c55-abac-8be0f8b213e4/kojiin-tamer"
bw = "https://bookwalker.jp/series/250142"

[chapters]
released = 22
read = 21
last_checked = 2021-10-30T00:00:00Z
+++
