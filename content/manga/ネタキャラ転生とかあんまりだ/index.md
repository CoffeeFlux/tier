+++
title = "ネタキャラ転生とかあんまりだ"
tags = []
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165603"
raw = "https://magcomi.com/episode/13933686331623324202"
md = "https://mangadex.org/title/fd41659a-e4ce-458f-b087-8032b7a5fab9/neta-chara-tensei-toka-anmarida"
bw = "https://bookwalker.jp/series/266743"

[chapters]
released = 13
read = 11
last_checked = 2022-02-15T00:00:00Z
+++

The art is kami; My initial gut reaction was "the faces look weird" but then i realized that it's so stylized that it's kino and good actually. Despite the work being rather similar to most others of its kind, the overall panelling/character deformation and general character writing carries it super hard to the point that I really enjoy this. I feel like it'd do really well as an anime if it managed to keep this artstyle intact somehow. This artist hasn't worked on anything else as far as I can see but I want them to since they're great. It's a great pairing overall with the tone/character writing of the series