+++
title = "老後に備えて異世界で８万枚の金貨を貯めます"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
tags = []
categories = ["isekai"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147516"
raw = "http://seiga.nicovideo.jp/comic/27620"
md = "https://mangadex.org/title/22843/saving-80-000-gold-coins-in-the-different-world-for-my-old-age"
bw = "https://bookwalker.jp/series/139071/"

[chapters]
released = 64
read = 62
last_checked = 2021-09-04T00:00:00Z
+++

badass competent protag destroys medieval peasants with Guns and Science