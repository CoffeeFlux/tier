+++
title = "奪う者 奪われる者"
tags = []
categories = []
demographics = ["seinen"]
statuses = ["axed"]
furigana = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=133712"
raw = "https://web-ace.jp/youngaceup/contents/1000021/"
md = "https://mangadex.org/title/ed95d175-0e50-4315-8044-3dccbcd63748/ubau-mono-ubawareru-mono"
bw = ""

[chapters]
released = 8.1
read = 8.1
last_checked = 2021-09-19T00:00:00Z
+++

It's possible that it might've been decent at some point, but it got axed before it ever really started so i'm not really sure where to put it, and since the artist started working on some separate series while he was working on this one they released like 6 pages a month for ages up until it finally got axed ruining the pacing.