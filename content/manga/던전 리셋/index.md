+++
title = "던전 리셋"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=163555"
raw = "https://page.kakao.com/home?seriesId=54471153"
md = "https://mangadex.org/title/45903/dungeon-reset"

[chapters]
released = 87
read = 0
last_checked = 2021-09-04T00:00:00Z
+++