+++
title = "デスマーチからはじまる異世界狂想曲"
categories = ["isekai"]
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=116685"
raw = "https://comic-walker.com/contents/detail/KDCW_FS02000014010000_68/"
md = "https://mangadex.org/title/14850/death-march-kara-hajimaru-isekai-kyousoukyoku"
bw = "https://bookwalker.jp/series/42937/"

[chapters]
released = 82
read = 80
last_checked = 2021-10-12T00:00:00Z
+++

it handwaves away way too fucking much, skips over tons of actual interesting content, and then you're mostly left with 'food of the week' and other banal shit. The protag is too excessively broken compared to everybody else to the point that it's not even interesting. It teased some large overarching plot relatively early on with some weird voodoo memories the protag had but then never touched upon it again for ages afterwards. The protag has a massive harem (of lolis) and despite them being super thirsty he doesn't want any of that, so it's just a constant annoying back and forth of 'please let me suck your dick protagkun', 'no', 'pien'. I do appreciate the other reincarnator along with him but at the same time she's kind of pointless. It takes a lot of effort to not just constantly frodopost while reading this.