+++
title = "大預言者は前世から逃げる"
tags = []
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=158897"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201131010000_68/"
md = "https://mangadex.org/title/1d6f3dd6-f222-40d4-aefb-f1b5b949b635/the-great-prophet-is-running-from-her-previous-life"
bw = "https://bookwalker.jp/de08757484-09a2-44a1-8009-b2e97a88073b/"

[chapters]
released = 20
read = 11
last_checked = 2021-12-13T00:00:00Z
+++

Technically this is an isekai, if you take the protagonist's first life into consideration, but considering that it's almost entirely glossed over and the 2nd life is more prominent to the story i've left the tag off. The first 3 chapters is basically the exact same information said over and over again. Also I legit thought the protag was a man in her first life since the artstyle is kind of weirdly ambiguous. The story is kind of really badly paced aand i'm not sure I like the writing in general. I'm not sure who to really rec this to. I'd rec 最強の黒騎士 instead of this every time since it's fairly similar in some ways but that series is actually good.