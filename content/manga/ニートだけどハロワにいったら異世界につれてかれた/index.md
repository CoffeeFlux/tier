+++
title = "ニートだけどハロワにいったら異世界につれてかれた"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147953"
raw = "https://www.comic-valkyrie.com/nitorowa/"
md = "https://mangadex.org/title/4d4db0c2-3c43-45c8-aac1-af2af63e85ff/neet-dakedo-hello-work-ni-ittara-isekai-ni-tsuretekareta"
bw = "https://bookwalker.jp/series/171219"

[chapters]
released = 39
read = 39
last_checked = 2021-10-06T00:00:00Z
+++

The title is meme. The protag is overpowered but also underpowered, and there's quite a fair amount of people who are exponentially stronger than he is in a lot of ways, but he's also stronger than a most people too, most of it through his own basic efforts despite having a 'cheat' ability. The harem speedrun is also a little meme, but it doesn't particularly do anything badly.