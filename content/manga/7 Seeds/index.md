+++
title = "7 Seeds"
statuses = ["completed"]
demographics = ["josei"]
furigana = ["full"]
tags = []
categories = ["isekai"]
sexual_contents = ["suggestive_sex", "attempted_rape"]
sources = ["original"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=548"
raw = "https://flowers.shogakukan.co.jp/rensai/7seeds.html"
md = "https://mangadex.org/title/402/7-seeds"
bw = "https://bookwalker.jp/series/128391/"

[chapters]
released = 178
read = 178
last_checked = 2021-09-04T00:00:00Z
+++

A group of misfits wake up in unknown surroundings on a strange planet without any recollection of how they got there, and have to figure out a way to survive. As they are just your average societal rejects most of them don't have much in the way of survival experience or overall knowledge to begin with, and aren't particularly physically fit. They're ill-suited to the environment and have to adapt while coping with the fact that they might never see their modern world again. 

If i'm being honest the art early on is painfully shoujo but the story is just that worth it to power through it regardless if you don't particularly like the art that much, and the art **does** improve over time. The perspective changes quite a lot across the various protagonists of the story so if you're not particularly fond of the first girl you're reading the perspective of know that there's others as well (and that she does improve). As a work it's fairly similar to the VN [Swan Song](https://vndb.org/v914) in a lot of ways, except that swan song is targeted towards seinen and 7seeds is targeted towards josei, which comes with its own tropes and plot elements stemming from that.

Vol10 is probably my favourite standalone volume out of the bunch fwiw.