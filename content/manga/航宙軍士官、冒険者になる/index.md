+++
title = "航宙軍士官、冒険者になる"
tags = []
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166643"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19200711010000_68/"
md = "https://mangadex.org/title/44553/the-galactic-navy-officer-becomes-an-adventurer"
bw = "https://bookwalker.jp/series/208811/"

[chapters]
released = 28.3
read = 23
last_checked = 2021-09-04T00:00:00Z
+++

An interesting take on fantasy isekai where it's actually "sci-fi" (cue wareya saying they're the same thing). Handled pretty competently although it kind of speedruns some of the "boring" sections and skims over them, but handles foreign languages decently enough. The pacing/flashbacks in the beginning is kind of awkward though.