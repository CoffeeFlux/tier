+++
title = "高1ですが異世界で城主はじめました"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=120279"
raw = "https://hobbyjapan.co.jp/comic/series/13/"
md = "https://mangadex.org/title/18753/kou-1-desu-ga-isekai-de-joushu-hajimemashita"
bw = "https://bookwalker.jp/series/56564/"

[chapters]
released = 43
read = 43
last_checked = 2021-09-04T00:00:00Z
+++

Always fun to have two people isekai at the same time, but the 2nd dude is kind of pointless, and a lot of the plot points are kind of mediocre. Main selling point of this manga is BOOBA. Would rec for BOOBA. Alternatively follow the artist's [twitter](https://twitter.com/rikak)/[pixiv](https://www.pixiv.net/en/users/3650)/[whatever](http://galvas.net/).