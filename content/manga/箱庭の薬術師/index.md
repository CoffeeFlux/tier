+++
title = "箱庭の薬術師"
tags = []
categories = ["isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=168695"
raw = "https://gaugau.futabanet.jp/list/work/5dd5003c77656175b3040000"
md = "https://mangadex.org/title/2a7ccd70-60e7-4ce6-820b-9adf4a9a0e40/boxed-garden"
bw = "https://bookwalker.jp/series/246587"

[chapters]
released = 17
read = 14
last_checked = 2021-12-05T00:00:00Z
+++

ch14/15 was unexpected but other than that it's pretty subpar