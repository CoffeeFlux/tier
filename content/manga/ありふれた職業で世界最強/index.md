+++
title = "ありふれた職業で世界最強"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=128108"
raw = "https://comic-gardo.com/episode/10834108156661711704"
md = "https://mangadex.org/title/18091/arifureta-shokugyou-de-sekai-saikyou"
bw = "https://bookwalker.jp/series/99891/"

[chapters]
released = 54
read = 53
last_checked = 2021-09-04T00:00:00Z
+++

Good comedy manga. Edgy NcEdgerton kills everyone with his edge. The art is great, but it skips a little too much that both the LN/Anime go over in detail. Useful as a supplement to one of the other mediums, but you might need to handwave some knowledge if you're reading it as the original. Gets most of it at least though.