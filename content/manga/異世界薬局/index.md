+++
title = "異世界薬局"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
tags = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=138217"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000031010000_68/"
md = "https://mangadex.org/title/20398/isekai-yakkyoku"
bw = "https://bookwalker.jp/series/107987/"

[chapters]
released = 35.2
read = 32
last_checked = 2021-09-04T00:00:00Z
+++