+++
title = "限界レベル１からの成り上がり"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=165513"
raw = "https://seiga.nicovideo.jp/comic/46451"
md = "https://mangadex.org/title/00b64b8f-cb7e-4322-9855-3669fe210ac7/genkai-level-1-kara-no-nariagari-saijaku-level-no-ore-ga-isekai-saikyou-ni-naru-made"
bw = "https://bookwalker.jp/series/264159"

[chapters]
released = 15
read = 14
last_checked = 2021-11-27T00:00:00Z
+++

Protag has trash level cap so gets thrown away immediately but he has Totally Epic Skill and Deus Ex Machina luck to make him just powerful enough to kill everything he meets without trouble. I'll give it bonus points for having a guild system that isn't letter ranks but a more vague 'trainee' -> 'regular' system that mostly acts as the militia/mercenary being shipped out to various warfields.