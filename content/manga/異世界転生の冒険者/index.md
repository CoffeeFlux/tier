+++
title = "異世界転生の冒険者"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=150166"
raw = "http://seiga.nicovideo.jp/comic/38271"
md = "https://mangadex.org/title/33616/isekai-tensei-no-boukensha"
bw = "https://bookwalker.jp/series/189726/"

[chapters]
released = 25.1
read = 0
last_checked = 2021-09-04T00:00:00Z
+++