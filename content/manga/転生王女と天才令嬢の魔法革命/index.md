+++
title = "転生王女と天才令嬢の魔法革命"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170415"
raw = "https://comic-walker.com/contents/detail/KDCW_AM01201792010000_68/"
md = "https://mangadex.org/title/46596dea-95de-40e8-b2b8-4e63aa6acd1a/the-magical-revolution-of-the-reincarnated-princess-and-the-genius-young-lady"
bw = "https://bookwalker.jp/https://bookwalker.jp/de5c1e3a53-2e23-459b-96f3-de237f797217/"

[chapters]
released = 13.2
read = 9
last_checked = 2021-09-20T00:00:00Z
+++

The facial expressions are great. The general art is great. The characters are great. This is kino.