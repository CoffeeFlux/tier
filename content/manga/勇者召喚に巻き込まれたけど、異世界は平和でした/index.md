+++
title = "勇者召喚に巻き込まれたけど、異世界は平和でした"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = ["none"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150176"
raw = "https://seiga.nicovideo.jp/comic/39510"
md = "https://mangadex.org/title/dfdd79ad-eae8-489f-8843-bfae7e08e74b/yuusha-shoukan-ni-makikomareta-kedo-isekai-wa-heiwa-deshita"
bw = "https://bookwalker.jp/series/201305"

[chapters]
released = 30
read = 29.5
last_checked = 2021-10-05T00:00:00Z
+++

the power of friendship prevails