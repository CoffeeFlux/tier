+++
title = "ライセカミカ"
statuses = ["completed"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=139119"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01000056010000_68/"
md = "https://mangadex.org/title/20624/raise-kamika"
bw = "https://bookwalker.jp/series/131854/"

[chapters]
released = 29
read = 29
last_checked = 2021-09-04T00:00:00Z
+++