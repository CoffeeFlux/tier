+++
title = "悪役令嬢の追放後！"
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=152938"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19200669010000_68/"
md = "https://mangadex.org/title/34844/the-banished-villainess-living-the-leisurely-life-of-a-nun-making-revolutionary-church-food"
bw = "https://bookwalker.jp/series/194419/"

[chapters]
released = 27
read = 17
last_checked = 2021-09-04T00:00:00Z
+++