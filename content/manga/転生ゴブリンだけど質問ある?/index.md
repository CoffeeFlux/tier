+++
title = "転生ゴブリンだけど質問ある?"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=163155"
raw = "https://tonarinoyj.jp/episode/10834108156765668108"
md = "https://mangadex.org/title/782211b2-44ca-44ae-89f2-8a1160221b96/tensei-goblin-dakedo-shitsumon-aru"
bw = "https://bookwalker.jp/series/250018"

[chapters]
released = 43
read = 28
last_checked = 2021-12-27T00:00:00Z
+++
