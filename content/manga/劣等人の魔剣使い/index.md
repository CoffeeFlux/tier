+++
title = "劣等人の魔剣使い"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=169080"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666634061"
md = "https://mangadex.org/title/50727/the-reincarnated-inferior-magic-swordsman"
bw = "https://bookwalker.jp/series/280578/"

[chapters]
released = 48
read = 38
last_checked = 2021-09-04T00:00:00Z
+++

The guild clerk is coom