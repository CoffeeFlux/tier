+++
title = "英雄の娘として生まれ変わった英雄は再び英雄を目指す"
statuses = ["axed"]
demographics = []
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=147335"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000063010000_68/"
md = "https://lovehug.net/402/"
bw = "https://bookwalker.jp/series/197294/"

[chapters]
released = 24.5
read = 24.5
last_checked = 2021-09-04T00:00:00Z
+++