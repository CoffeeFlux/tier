+++
title = "美女と賢者と魔人の剣"
tags = []
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=146715"
raw = "http://seiga.nicovideo.jp/comic/36004"
md = "https://mangadex.org/title/e7e950c1-bbc3-4579-a659-ac9ffacc4ab2/bijo-to-kenja-to-majin-no-ken"
bw = "https://bookwalker.jp/series/214922"

[chapters]
released = 17
read = 15
last_checked = 2021-12-12T00:00:00Z
+++

This series would actually probably be good if it weren't for the extremely over the top pointless sexualization of all of the female characters that's prevalent across the entire work.