+++
title = "悪役令嬢なのでラスボスを飼ってみました"
statuses = ["completed"]
demographics = ["shoujo"]
furigana = ["full"]
tags = []
categories = []
sexual_contents = [""]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151722"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04200444010000_68/"
md = "https://mangadex.org/title/35064/i-m-a-villainous-daughter-so-i-m-going-to-keep-the-last-boss"
bw = "https://bookwalker.jp/series/182093/"

[chapters]
released = 13.5
read = 13.5
last_checked = 2021-09-04T00:00:00Z
+++

Relatively short yet competent "villianess" manga. I feel like with its length it's worth trying out regardless just to get a feel for the genre, and even if you're familiar with the genre it's worth it.