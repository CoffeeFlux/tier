+++
title = "ウイルス転生から始まる異世界感染物語"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=173324"
raw = "https://seiga.nicovideo.jp/comic/49920"
md = "https://mangadex.org/title/29b7cf38-367e-4785-a7a0-3e92e57b09ef/virus-tensei-kara-hajimaru-isekai-kansen-monogatari"
bw = "https://bookwalker.jp/series/295919/"

[chapters]
released = 13.2
read = 13.1
last_checked = 2021-09-21T00:00:00Z
+++

A major draw of this work is the cute heroine which is why she's front and center of all of the volume covers.