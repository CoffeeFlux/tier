+++
title = "今際の国のアリス"
statuses = ["completed"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=59540"
md = "https://mangadex.org/title/2890/alice-in-borderland"
bw = "https://bookwalker.jp/series/124293/"

[chapters]
released = 65
read = 0
last_checked = 2021-09-04T00:00:00Z
+++