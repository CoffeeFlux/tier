+++
title = "ちびっこ賢者、Lv.1から異世界でがんばります"
tags = []
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153413"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19200714010000_68/"
md = "https://mangadex.org/title/35573/the-small-sage-will-try-her-best-in-the-different-world-from-lv-1"
bw = "https://bookwalker.jp/series/210400/"

[chapters]
released = 26
read = 26
last_checked = 2021-09-04T00:00:00Z
+++
