+++
title = "黒の創造召喚師"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=144014"
raw = "https://www.alphapolis.co.jp/manga/official/194000183"
md = "https://mangadex.org/title/a72994fd-f1c5-4df2-b7b0-423356408958/kuro-no-souzou-shoukanshi-tenseisha-no-hangyaku"
bw = "https://bookwalker.jp/series/161013/"

[chapters]
released = 26
read = 26
last_checked = 2021-09-29T00:00:00Z
+++

i was going to put this in F but ch24 is actually kind of good so i'll give it a D.