+++
title = "異世界を制御魔法で切り開け！"
tags = []
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=133671"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/255000138"
md = "https://mangadex.org/title/ea3d37dc-e694-4535-a7cb-dfb43c71a897/isekai-wo-seigyo-mahou-de-kirihirake"
bw = "https://bookwalker.jp/series/101305"

[chapters]
released = 36
read = 36
last_checked = 2022-01-27T00:00:00Z
+++

Okay so, isekai is in the title of this one, but it's not really isekai at all, which is baffling. Maybe it delves more into being isekai in the WN or whatever but at least it's not one in this manga adaptation. Additionally the manga adaptation becomes utterly butchered near the end where the last 10 chapters or so are just rushed nonsense. I enjoyed the beginning chapters but over time it just unfortunately became worse and worse until it got seemingly axed. Maybe the WN/LN is better?