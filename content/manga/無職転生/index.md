+++
title = "無職転生"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=110083"
raw = "https://comic-walker.com/contents/detail/KDCW_MF01000009010000_68"
md = "https://mangadex.org/title/12108/mushoku-tensei-isekai-ittara-honki-dasu"
bw = "https://bookwalker.jp/series/153747/"

[chapters]
released = 74
read = 0
last_checked = 2021-09-04T00:00:00Z
+++