+++
title = "LV2からチートだった元勇者候補のまったり異世界ライフ"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = []
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=152684"
raw = "https://comic-gardo.com/episode/10834108156661709946"
md = "https://mangadex.org/title/33797/lv2-kara-cheat-datta-moto-yuusha-kouho-no-mattari-isekai-life"
bw = "https://bookwalker.jp/series/211352/"

[chapters]
released = 27
read = 26
last_checked = 2021-09-04T00:00:00Z
+++