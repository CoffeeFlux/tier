+++
title = "善人おっさん、生まれ変わったらSSSランク人生が確定した"
tags = []
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156775"
raw = "https://seiga.nicovideo.jp/comic/40678"
md = "https://mangadex.org/title/84a16155-afda-47d5-9aec-606e46307231/your-sss-rank-afterlife-is-confirmed-virtuous-old-man"
bw = "https://bookwalker.jp/series/218551"

[chapters]
released = 57
read = 19
last_checked = 2021-10-17T00:00:00Z
+++
