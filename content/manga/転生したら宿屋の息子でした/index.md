+++
title = "転生したら宿屋の息子でした"
tags = []
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=176790"
raw = "https://magazine.jp.square-enix.com/mangaup/original/tensei_yadoya/"
md = "https://mangadex.org/title/18ce2342-f970-4fc4-bb2e-398577200879/tensei-shitara-yadoya-no-musuko-deshita-inakagai-de-nonbiri-slow-life-o-okurou"
bw = "https://bookwalker.jp/series/303286/list/"

[chapters]
released = 9
read = 9
last_checked = 2022-02-12T00:00:00Z
+++

Dude gets overworked at black company and then reincarnates and then decides to be as lazy/sleepy as possible to make up for his lifetime of barely getting any sleep in his previous life, but gets reluctantly forced to do things constantly by his family because obviously being a freeloader is bad. the end.