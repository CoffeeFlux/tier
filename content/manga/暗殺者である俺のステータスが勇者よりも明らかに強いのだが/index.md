+++
title = "暗殺者である俺のステータスが勇者よりも明らかに強いのだが"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=149362"
raw = "https://comic-gardo.com/episode/10834108156661710102"
md = "https://mangadex.org/title/28901/assassin-de-aru-ore-no-sutetasu-ga-yuusha-yori-mo-akiraka-ni-tsuyoi-nodaga"
bw = "https://bookwalker.jp/series/189259/"

[chapters]
released = 20
read = 0
last_checked = 2021-09-04T00:00:00Z
+++