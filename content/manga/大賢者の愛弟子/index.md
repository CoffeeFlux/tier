+++
title = "大賢者の愛弟子"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = ["nudity", "ecchi"]
sources = ["web_novel"]

[links]
mu = "https://www.mangaupdates.com/series.html?id=163435"
raw = "https://seiga.nicovideo.jp/comic/46807"
md = "https://mangadex.org/title/d506f85f-f318-42bc-84fa-a651a3bae4e4/great-wise-man-s-beloved-pupil"
bw = "https://bookwalker.jp/series/260144/list/"

[chapters]
released = 14.2
read = 10.2
last_checked = 1970-01-01T00:00:00Z
+++

It's an reincarnation/isekai but the protagonist is not the one who reincarnated, the protagonist is his rival instead, which is interesting enough in its own right. It's about a badass using magic in unconventional ways hoping people can learn about how absolutely awesome this style of magic is, while people look down on him for using what is normally considered to be beginner's magic. It dips into ecchi stuff relatively frequently as well I guess.