+++
title = "けものみち"
tags = []
categories = []
demographics = []
statuses = []
furigana = []
sexual_contents = []
sources = []

[links]
mu = ""
raw = ""
md = ""
bw = ""

[chapters]
released = 0
read = 0
last_checked = 1970-01-01T00:00:00Z
+++

The manga speedruns past the intro and immediatelly just shoves you right into the thick of things; you're probably better of watching the earlier episodes of the anime or reading the LN/etc and then picking up back to the manga afterwards. Basically, ch1 of the manga covers up until ep4 of the anime for the most part, just completely skipping over a lot of stuff.