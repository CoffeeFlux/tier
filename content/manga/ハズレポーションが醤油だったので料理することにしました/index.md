+++
title = "ハズレポーションが醤油だったので料理することにしました"
tags = []
categories = ["isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["none"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=156685"
raw = "https://gaugau.futabanet.jp/list/work/5dce92a87765614c0a010000"
md = "https://mangadex.org/title/1fb6e5d7-35b8-4741-9fca-2426f6c37e31/hazure-potion-ga-shouyu-datta-no-de-ryouri-suru-koto-ni-shimashita"
bw = "https://bookwalker.jp/series/200754/"

[chapters]
released = 23
read = 10
last_checked = 2022-01-11T00:00:00Z
+++

dabbing on isekai people with superior japanese cuisine, but this time with extremely broken status effects on top of the dishes. What were they eating before you ask? Stale bread. What savages they are, unlike the common japanese person who enjoys rice and soy sauce.