+++
title = "劣等眼の転生魔術師"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=153684"
raw = "http://seiga.nicovideo.jp/comic/37760"
md = "https://mangadex.org/title/36284/the-reincarnation-magician-of-the-inferior-eyes"
bw = "https://bookwalker.jp/series/194756/"

[chapters]
released = 60
read = 58
last_checked = 2021-09-04T00:00:00Z
+++