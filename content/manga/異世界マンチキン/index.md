+++
title = "異世界マンチキン"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=154522"
raw = "http://seiga.nicovideo.jp/comic/39410"
md = "https://mangadex.org/title/39256/otherworldly-munchkin-let-s-speedrun-the-dungeon-with-only-1-hp"
bw = "https://bookwalker.jp/series/218876/"

[chapters]
released = 53
read = 52
last_checked = 2021-09-04T00:00:00Z
+++