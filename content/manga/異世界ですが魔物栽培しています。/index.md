+++
title = "異世界ですが魔物栽培しています。"
tags = []
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=144880"
raw = "http://comic-walker.com/contents/detail/KDCW_MF00000040010000_68"
md = "https://mangadex.org/title/1ed075e6-432a-4e21-a4f6-d63e4c5ae485/isekai-desu-ga-mamono-saibai-shiteimasu"
bw = "https://bookwalker.jp/series/135229"

[chapters]
released = 35
read = 35
last_checked = 2022-02-01T00:00:00Z
+++

Protag shoves stuff in ground. Stuff comes out of ground. You can't explain that. I really appreciate the double page in ch27 though