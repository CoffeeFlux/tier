+++
title = "植物魔法チートでのんびり領主生活始めます"
tags = []
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sexual_contents = [""]
sources = []

[links]
mu = "https://www.mangaupdates.com/series.html?id=174958"
raw = "https://comic-walker.com/contents/detail/KDCW_FS00201833010000_68"
md = "https://mangadex.org/title/030157c5-08d1-44c9-b0ed-9d9234f96dff/shokubutsu-mahou-chito-de-nonbiri-ryoshu-seikatsu-hajimemasu-zense-no-chishiki-o-kushi-shite-nogyo"
bw = "https://bookwalker.jp/series/295158/"

[chapters]
released = 16
read = 12
last_checked = 2022-02-13T00:00:00Z
+++

reincarnates as a dude with "useless" plant magic, regains his memories from his previous life when he played this mmo that this world is based off of, then uses the useless plant magic to live a hippie lifestyle surrounded by trees and magical creatures like a disney princess. The rules around his magic is kind of dumb since he can make super large tree buildings but he can't making a simple fucking signboard because his skill only allows him to make things he understands(?). He understands a house and every object inside of it but can't make a signboard?