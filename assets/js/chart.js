let items = document.querySelectorAll(".chart .items");        
let checkboxes = document.querySelectorAll(".term input");

function filtered_handle(iso) {
    let num_elements = iso.getFilteredItemElements().length;
    let parent = iso.element.parentNode;

    let entries = parent.querySelector("th small:nth-of-type(2)");
    entries.textContent = "(" + num_elements + " entries)";
}

function join_inclusive(arr) {
    return arr.length ? arr.join(', ') : '*';
}

function join_combine(arr) {
    return arr.length ? arr.join('') : '*';
}

function arrange() {
    let filtered = [];
    
    checkboxes.forEach((value, key, parent) => {
        if ( value.checked ) {
            filtered.push( value.value );
        }
    });

    let filterValue = join_combine(filtered);

    for(let iso in isos) {
        isos[iso].arrange({ filter: filterValue });
        filtered_handle(isos[iso]);
    }
}

let isos = [];
items.forEach((value, key, parent) => {
    let iso = new Isotope( value, {
            itemSelector: '.chart-item',
            layoutMode: 'masonry'
        });
    isos.push(iso);

    imagesLoaded(value, function(instance) {
        arrange();
    });
});

checkboxes.forEach((value, key, parent) => {
    value.addEventListener('change', (event) => {
        let is_checked = event.target.checked;
        let name = value.getAttribute("name");
        document.querySelectorAll('input[name="'+name+'"]').forEach((other, key, parent) => {
            other.checked = false;
        })
        event.target.checked = is_checked;
        arrange();
    })
});